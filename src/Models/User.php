<?php

namespace Panda\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends Base implements AuthenticatableContract
{
    use Authenticatable;

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'telephone',
        'password',
        'address_id',
        'remember_token',
    ];

    public function isAdmin()
    {
        return $this->groups->contains('id', Group::GROUP_ADMIN);
    }

    public function address()
    {
        return $this->belongsTo('Panda\Models\Address');
    }

    public function groups()
    {
        return $this->belongsToMany('Panda\Models\Group');
    }

    public function orders()
    {
        return $this->hasMany('Panda\Models\Order');
    }
}
