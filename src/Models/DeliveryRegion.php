<?php

namespace Panda\Models;

class DeliveryRegion extends Base
{
    protected $fillable = [
    	'id',
    	'region',
    	'delivery',
    	'courier'
    ];
}
