<?php

namespace Panda\Models;

class FieldType extends Base
{
    const FIELD_TYPE_TEXT_FIELD = 4;

    protected $fillable = [
        'name',
        'class'
    ];
}
