<?php

namespace Panda\Models;

class OrderItem extends Base
{
    protected $fillable = [
        'item',
        'lot_number',
        'email',
        'price',
        'page_id',
        'order_id',
    ];
}
