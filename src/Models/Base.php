<?php

namespace Panda\Models;

use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use Panda\Traits\PublishedTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Base extends Model
{
    use PublishedTrait;

    public function scopeOnline(Builder $query)
    {
        return $query->where('status', 1);
    }

    public function scopeOrder(Builder $query)
    {
        return $query;
    }

    public function editUrl()
    {
        try {
            $route = $this instanceof Page
                ? route('admin.'.$this->getTable().'.edit', [$this->template->name, $this->id])
                : route('admin.'.$this->getTable().'.edit', $this->id);

            return $route;
        } catch(InvalidArgumentException $e) {
            Log::error($e->getMessage());
        }
    }

    public function indexUrl()
    {
        try {
            $route = $this instanceof Page
                ? route('admin.'.$this->getTable().'.index', $this->template->name)
                : route('admin.'.$this->getTable().'.index');

            return $route;
        } catch(InvalidArgumentException $e) {
            Log::error($e->getMessage());
        }
    }
}
