<?php

namespace Panda\Models;

class Address extends Base
{
    protected $fillable = [
        'address_line_1',
        'address_line_2',
        'city',
        'county',
        'country',
        'postcode',
        'user_id'
    ];
}
