<?php

namespace Panda\Models;

class Group extends Base
{
    const GROUP_ADMIN = 1;

    protected $fillable = [
        'name'
    ];
}