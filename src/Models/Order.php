<?php

namespace Panda\Models;

class Order extends Base
{
    protected $fillable = [
        'reference',
        'first_name',
        'last_name',
        'email',
        'telephone',
        'price',
        'delivery_price',
        'sent',
        'paid',
        'user_id',
        'address_id',
        'delivery_option_id',
        'delivery_region_id',
    ];

    public function items()
    {
        return $this->hasMany('Panda\Models\OrderItem');
    }

    public function address()
    {
        return $this->belongsTo('Panda\Models\Address');
    }

    public function deliveryOption()
    {
        return $this->belongsTo('Panda\Models\DeliveryOption');
    }

    public function deliveryRegion()
    {
        return $this->belongsTo('Panda\Models\DeliveryRegion');
    }

    public function payment()
    {
        return $this->hasOne('Panda\Models\Payment');
    }
}
