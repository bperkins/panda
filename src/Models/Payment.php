<?php

namespace Panda\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Base
{
    protected $fillable = [
    	'order_id',
    	'value'
    ];
}
