<?php

namespace Panda\Models;

class TemplateType extends Base
{
    const PAGE = 1;
    const BLOCK = 2;

    protected $fillable = [
        'name'
    ];
}