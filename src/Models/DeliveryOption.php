<?php

namespace Panda\Models;

class DeliveryOption extends Base
{
    protected $fillable = [
    	'id',
    	'name',
    	'label'
    ];
}
