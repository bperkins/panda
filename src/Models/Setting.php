<?php

namespace Panda\Models;

class Setting extends Base
{
    const SETTING_SITE_NAME = 'site_name';

    protected $fillable = [
        'name',
        'label',
        'value',
        'field_type_id'
    ];
}
