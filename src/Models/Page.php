<?php

namespace Panda\Models;

use Panda\Traits\NestableTrait;
use stdClass;

class Page extends Base
{
    use NestableTrait;

    protected $fillable = [
        'slug',
        'uri',
        'title',
        'status',
        'position',
        'is_home',
        'template_id',
        'parent_id',
        'meta_title',
        'meta_description',
        'meta_keywords'
    ];

    protected $appends = [
        'fields'
    ];

    public function getFieldsAttribute()
    {
        return $this->getFields($this);
    }

    public function values()
    {
        return $this->belongsToMany('Panda\Models\FieldValue');
    }

    public function parent()
    {
        return $this->belongsTo('Panda\Models\Page', 'parent_id');
    }

    public function template()
    {
        return $this->belongsTo('Panda\Models\Template');
    }

    public function updatedBy()
    {
        return $this->belongsTo('Panda\Models\User', 'updated_by');
    }

    public function getFields($model)
    {
        $data = new stdClass();

        // Get all fields associated with the given template.
        $fields = $model->template->fields;

        // Get all values associated with the given model.
        $values = $model->values->pluck('value', 'field_id');

        // Loop through the fields so we can assign values.
        foreach($fields as $field) {

            // If the value doesn't exist, set the value to null.
            $value = isset($values[$field->id]) ? $values[$field->id] : null;

            // Try to decode the value in-case it's JSON.
            $decode = json_decode($value);

            // Check to see if the decode worked, if not just reassign the value as it wasn't JSON.
            $value = $decode ? $decode : $value;

            // Check to see if the field is the block field.
            if($value && $field->field_type_id == 1) {

                $blocks = [];

                $value = is_array($value) ? $value : [$value];

                // We need to loop through the blocks to get the fields/values.
                foreach($value as $blockId) {

                    // Find the given block.
                    //$block = $this->blockRepository->find($blockId);
                    $block = Block::find($blockId);

                    if($block) {

                        // Block found, lets decode the fields and add it to the value.
                        $block->fields = $this->getFields($block);

                        $blocks[] = $block;
                    }
                }

                $value = $blocks;
            }

            $data->{$field->name} = $value;
        }

        return $data;
    }

    public function getFirstByUri($uri)
    {
        $uri = $uri !== '/' ? '/'.$uri : $uri;

        return $this
            ->where('uri', $uri)
            ->where('status', 1)
            ->firstOrFail();
    }
}
