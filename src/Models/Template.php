<?php

namespace Panda\Models;

class Template extends Base
{
    const TEMPLATE_PRODUCT = 5;

    protected $fillable = [
        'name',
        'label',
        'icon',
        'visible',
        'template_type_id'
    ];

    public function type()
    {
        return $this->hasOne('Panda\Models\TemplateType', 'id', 'template_type_id');
    }

    public function fields()
    {
        return $this->hasMany('Panda\Models\Field');
    }

    public function pages()
    {
    	return $this->hasMany('Panda\Models\Page');
    }

    public function blocks()
    {
        return $this->hasMany('Panda\Models\Block');
    }

    public function parents()
    {
        return $this->belongsToMany('Panda\Models\Page');
    }
}