<?php

namespace Panda\Repositories;

use Panda\Repositories\Eloquent\BaseRepository;

class SettingRepository extends BaseRepository
{
    public function model()
    {
        return 'Panda\Models\Setting';
    }

    public function getSettingByName($name)
    {
    	return $this->findByField('name', $name);
    }
}
