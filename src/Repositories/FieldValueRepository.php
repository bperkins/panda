<?php

namespace Panda\Repositories;

use Panda\Repositories\Eloquent\BaseRepository;

class FieldValueRepository extends BaseRepository
{
    function model()
    {
        return 'Panda\Models\FieldValue';
    }
}
