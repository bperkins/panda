<?php

namespace Panda\Repositories;

use Panda\Repositories\Eloquent\BaseRepository;

class GroupRepository extends BaseRepository
{
    function model()
    {
        return 'Panda\Models\Group';
    }
}