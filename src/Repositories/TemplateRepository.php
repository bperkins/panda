<?php

namespace Panda\Repositories;

use Panda\Repositories\Eloquent\BaseRepository;

class TemplateRepository extends BaseRepository
{
	function model()
	{
		return 'Panda\Models\Template';
	}
}