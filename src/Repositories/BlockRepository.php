<?php

namespace Panda\Repositories;

use Panda\Repositories\Eloquent\BaseRepository;

class BlockRepository extends BaseRepository
{
	function model()
	{
		return 'Panda\Models\Block';
	}

    public function setFieldValues($id, array $values)
    {
        $block = $this->find($id);

        foreach($values as $id => $value) {

            if(is_array($value)) {
                $value = json_encode($value);
            }

            $block->values()->updateOrCreate(['field_id' => $id], ['value' => $value, 'field_id' => $id]);
        }
    }
}
