<?php

namespace Panda\Repositories;

use Panda\Repositories\Eloquent\BaseRepository;

class DeliveryOptionRepository extends BaseRepository
{
    function model()
    {
        return 'Panda\Models\DeliveryOption';
    }

    public function getList()
    {
    	return $this->orderBy('label')
    		->all()
            ->pluck('label', 'id')
            ->toArray();
    }

    public function exists($deliveryOptionId)
    {
    	return $this->find($deliveryOptionId)->exists();
    }
}
