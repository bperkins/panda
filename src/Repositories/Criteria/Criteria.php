<?php

namespace Panda\Repositories\Criteria;

use Panda\Repositories\Contracts\RepositoryInterface;

abstract class Criteria
{
    abstract public function apply($model, RepositoryInterface $repository);
}