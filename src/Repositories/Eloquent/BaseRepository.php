<?php

namespace Panda\Repositories\Eloquent;

use Closure;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Container\Container as App;
use Panda\Models\Page;
use Panda\Repositories\Contracts\CriteriaInterface;
use Panda\Repositories\Contracts\RepositoryInterface;

abstract class BaseRepository implements RepositoryInterface, CriteriaInterface
{
    protected $app;
    protected $model;
    protected $criteria;
    protected $skipCriteria = false;
    protected $scopeQuery = null;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->criteria = new Collection();
        $this->makeModel();
    }

    public function resetModel()
    {
        $this->makeModel();
    }

    abstract public function model();

    public function makeModel()
    {
        return $this->model = $this->app->make($this->model());
    }

    public function scopeQuery(Closure $scope)
    {
        $this->scopeQuery = $scope;

        return $this;
    }

    public function lists($column, $key = null)
    {
        $model = $this->makeModel()->pluck($column, $key);

        return $model;
    }

    public function all()
    {
        $this->applyCriteria();
        $this->applyScope();

        if ($this->model instanceof Builder) {
            $results = $this->model->get();
        } else {
            $results = $this->model->all();
        }

        $this->resetModel();
        $this->resetScope();

        return $results;
    }

    public function first()
    {
        $this->applyCriteria();
        $this->applyScope();

        $results = $this->model->first();

        $this->resetModel();

        return $results;
    }

    public function paginate($limit = null)
    {
        $this->applyCriteria();
        $this->applyScope();

        $limit = request()->get('perPage', is_null($limit) ? 15 : $limit);

        $results = $this->model->paginate($limit);
        $results->appends(request()->query());

        $this->resetModel();

        return $results;
    }

    public function find($id)
    {
        $this->applyCriteria();
        $this->applyScope();

        $model = $this->model->find($id);

        $this->resetModel();

        return $model;
    }

    public function findByField($field, $value = null)
    {
        $this->applyCriteria();
        $this->applyScope();

        $model = $this->model->where($field, '=', $value)->get();

        $this->resetModel();

        return $model;
    }

    public function findWhere(array $where)
    {
        $this->applyCriteria();
        $this->applyScope();

        foreach ($where as $field => $value) {
            if (is_array($value)) {
                list($condition, $val) = $value;
                $this->model = $this->model->where($field, $condition, $val);
            } else {
                $this->model = $this->model->where($field, '=', $value);
            }
        }

        $model = $this->model->get();

        $this->resetModel();

        return $model;
    }

    public function findWhereIn($field, array $values)
    {
        $this->applyCriteria();

        $model = $this->model->whereIn($field, $values)->get();

        $this->resetModel();

        return $model;
    }

    public function findWhereNotIn($field, array $values)
    {
        $this->applyCriteria();

        $model = $this->model->whereNotIn($field, $values)->get();

        $this->resetModel();

        return $model;
    }

    public function create(array $attributes)
    {
        if (isset($attributes['fields'])) {
            unset($attributes['fields']);
        }

        $model = $this->model->newInstance($attributes);
        $model->save();

        $this->resetModel();

        return $model;
    }

    public function update(array $attributes, $id)
    {
        if (isset($attributes['fields'])) {
            unset($attributes['fields']);
        }

        $this->applyScope();

        $model = $this->model->withUnpublished()->findOrFail($id);
        $model->fill($attributes);
        
        $model->save();

        $this->resetModel();

        return $model;
    }

    public function updateOrCreate(array $attributes, array $values = [])
    {
        $this->applyScope();

        $model = $this->model->updateOrCreate($attributes, $values);

        $this->resetModel();

        return $model;
    }

    public function delete($id)
    {
        $this->applyScope();

        $model = $this->model->findOrFail($id);

        $this->resetModel();

        return $model->delete();
    }

    public function has($relation)
    {
        $this->model = $this->model->has($relation);

        return $this;
    }

    public function with(array $relations)
    {
        $this->model = $this->model->with($relations);

        return $this;
    }

    public function hidden(array $fields)
    {
        $this->model->setHidden($fields);

        return $this;
    }

    public function orderBy($column, $direction = 'asc')
    {
        $this->model = $this->model->query()->orderBy($column, $direction);

        return $this;
    }

    public function visible(array $fields)
    {
        $this->model->setVisible($fields);

        return $this;
    }

    public function pushCriteria($criteria)
    {
        if (is_string($criteria)) {
            $criteria = new $criteria;
        }

        $this->criteria->push($criteria);

        return $this;
    }

    public function popCriteria($criteria)
    {
        $this->criteria = $this->criteria->reject(function ($item) use ($criteria) {
            if (is_object($item) && is_string($criteria)) {
                return get_class($item) === $criteria;
            }

            if (is_string($item) && is_object($criteria)) {
                return $item === get_class($criteria);
            }

            return get_class($item) === get_class($criteria);
        });

        return $this;
    }

    public function getCriteria()
    {
        return $this->criteria;
    }

    public function getByCriteria(CriteriaInterface $criteria)
    {
        $this->model = $criteria->apply($this->model, $this);

        $results = $this->model->get();

        $this->resetModel();

        return $results;
    }

    public function skipCriteria($status = true)
    {
        $this->skipCriteria = $status;

        return $this;
    }

    public function resetCriteria()
    {
        $this->criteria = new Collection();

        return $this;
    }

    public function resetScope()
    {
        $this->scopeQuery = null;

        return $this;
    }

    protected function applyScope()
    {
        if (isset($this->scopeQuery) && is_callable($this->scopeQuery)) {
            $this->model = $this->scopeQuery($this->model);
        }

        return $this;
    }

    protected function applyCriteria()
    {
        if ($this->skipCriteria === true) {
            return $this;
        }

        $criteria = $this->getCriteria();

        if ($criteria) {
            foreach($criteria as $item) {
                if ($item instanceof CriteriaInterface) {
                    $this->model = $item->apply($this->model, $this);
                }
            }
        }

        return $this;
    }
}
