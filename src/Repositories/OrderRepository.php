<?php

namespace Panda\Repositories;

use Panda\Repositories\Eloquent\BaseRepository;

class OrderRepository extends BaseRepository
{
    function model()
    {
        return 'Panda\Models\Order';
    }
}
