<?php

namespace Panda\Repositories;

use Panda\Repositories\Eloquent\BaseRepository;

class AddressRepository extends BaseRepository
{
    function model()
    {
        return 'Panda\Models\Address';
    }
}
