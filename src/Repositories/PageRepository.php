<?php

namespace Panda\Repositories;

use Panda\Repositories\Eloquent\BaseRepository;
use App\Http\Controllers\BaseController;

class PageRepository extends BaseRepository
{
    const FIELD_SOLD = 19;
    const FIELD_FEATURED = 42;

    function model()
    {
        return 'Panda\Models\Page';
    }
    
    public function findByTemplate($id)
    {
        return $this->findByField('template_id', $id);
    }

    public function setFieldValues($id, array $values)
    {
        $page = $this->makeModel()->withUnpublished()->where('id', $id)->first();

        foreach($values as $id => $value) {

            if(is_array($value)) {
                $value = json_encode($value);
            }

            $page->values()->updateOrCreate(['field_id' => $id], ['value' => $value, 'field_id' => $id]);
        }
    }

    public function getFeatured()
    {
        $featured = [];

        $featuredIds = $this->makeModel()
            ->select('pages.id')
            ->join('field_value_page AS fvp', 'pages.id', '=', 'fvp.page_id')
            ->join('field_values AS fv', 'fv.id', '=', 'fvp.field_value_id')
            ->where('fv.field_id', 42)
            ->where('fv.value', '<>', NULL)
            ->get()
            ->pluck('id')
            ->toArray();

        $featured = $this->findWhereIn('id', $featuredIds)->sortBy(function ($product) {
            return $product->fields->featured;
        });

        return $featured;
    }

    public function setSold($pages, $status)
    {
        foreach ($pages as $page) {
            if ($page->template_id == BaseController::TEMPLATE_PRODUCTS) {
                $page->values()
                    ->where('field_id', '=', self::FIELD_SOLD)
                    ->update([
                        'value' => $status ? 1 : 0
                    ]);
            }
        }
    }
}
