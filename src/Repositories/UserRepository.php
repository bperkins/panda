<?php

namespace Panda\Repositories;

use Panda\Repositories\Eloquent\BaseRepository;

class UserRepository extends BaseRepository
{
    function model()
    {
        return 'Panda\Models\User';
    }

    public function create(array $attributes)
    {
        if (isset($attributes['password'])) {
            $attributes['password'] = bcrypt($attributes['password']);
        }

        $attributes['remember_token'] = str_random(32);

        $model = $this->model->newInstance($attributes);
        $model->save();

        $this->resetModel();

        return $model;
    }

    public function update(array $attributes, $id)
    {
        $this->applyScope();

        $model = $this->model->findOrFail($id);

        if (empty($attributes['password'])) {
            unset($attributes['password']);
        }

        if (isset($attributes['password'])) {
            $attributes['password'] = bcrypt($attributes['password']);
        }

        $model->fill($attributes);
        $model->save();

        $this->resetModel();

        return $model;
    }
}
