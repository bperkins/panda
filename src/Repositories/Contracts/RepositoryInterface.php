<?php

namespace Panda\Repositories\Contracts;

use Closure;

interface RepositoryInterface
{
    public function lists($column, $key = null);

    public function all();

    public function first();

    public function paginate($limit = null);

    public function find($id);

    public function findByField($field, $value);

    public function findWhere(array $where);

    public function findWhereIn($field, array $where);

    public function findWhereNotIn($field, array $where);

    public function create(array $attributes);

    public function update(array $attributes, $id);

    public function updateOrCreate(array $attributes, array $values = []);

    public function delete($id);

    public function orderBy($column, $direction = 'asc');

    public function with(array $relations);

    public function hidden(array $fields);

    public function visible(array $fields);

    public function scopeQuery(Closure $scope);

    public function resetScope();
}
