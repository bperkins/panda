<?php

namespace Panda\Repositories\Contracts;

interface CriteriaInterface
{
    public function pushCriteria($criteria);

    public function popCriteria($criteria);

    public function getCriteria();

    public function getByCriteria(CriteriaInterface $criteria);

    public function skipCriteria($status = true);

    public function resetCriteria();
}
