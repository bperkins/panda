<?php

namespace Panda\Repositories;

use Panda\Repositories\Eloquent\BaseRepository;

class DeliveryRegionRepository extends BaseRepository
{
	function model()
	{
		return 'Panda\Models\DeliveryRegion';
	}

	public function getList()
	{
		return $this->orderBy('region')
            ->all()
            ->pluck('region', 'id')
            ->toArray();
	}
}
