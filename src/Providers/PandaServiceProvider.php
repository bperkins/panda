<?php

namespace Panda\Providers;

use Illuminate\Support\ServiceProvider;
use Panda\Commands\DatabaseCommand;
use Panda\Commands\InstallCommand;
use Panda\Commands\ElasticIndexCommand;
use Panda\Events\ResetChildrenEvent;
use Panda\Models\File;
use Panda\Models\Page;
use Panda\Observers\CacheObserver;
use Panda\Observers\FileObserver;
use Panda\Observers\HomePageObserver;
use Panda\Observers\TouchObserver;
use Panda\Observers\UriObserver;
use Panda\Services\FileUpload;

class PandaServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'panda');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang','panda');

        $this->publishes([
            __DIR__.'/../config/panda.php' => config_path('panda.php'),
        ]);

        $this->commands('command.install');
        $this->commands('command.elastic.index');

        $this->loadComposers();

        Page::observe(new UriObserver());
        Page::observe(new TouchObserver());
    }

    public function register()
    {
        $this->registerProviders();
        $this->registerAliases();
        $this->registerCommands();
    }

    private function registerProviders()
    {
        $app = $this->app;

        $app->register('Panda\Providers\RouteServiceProvider');
        $app->register('Barryvdh\Elfinder\ElfinderServiceProvider');
    }

    private function registerAliases()
    {
        $app = $this->app;

        $app->alias('Pages', 'Panda\Facades\PageFacade');
    }

    private function loadComposers()
    {
        view()->composer(
            'panda::partials.layouts.sidebar', 'Panda\Composers\SidebarComposer'
        );
    }

    private function registerCommands()
    {
        $app = $this->app;

        $app->bind('command.install', function () {
            return new InstallCommand();
        });

        $app->bind('command.elastic.index', function () {
            return new ElasticIndexCommand();
        });
    }
}
