<?php

namespace Panda\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Panda\Models\Template;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The default controller store.
     *
     * @var string
     */
    protected $namespace = 'Panda\Http\Controllers';

    /**
     * Binds the URI parameter to check if the page exists.
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        parent::boot($router);

        $router = $this->app->make(Router::class);

        $router->bind('template', function ($template) {
            return Template::where('name', $template)->firstOrFail();
        });
    }

    /**
     * All CMS routes using the global controller namespace.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function(Router $router) {

            /**
             * AuthController
             */
            $router->get('admin', ['as' => 'admin.auth.index', 'uses' => 'Users\AuthController@index']);
            $router->post('admin', ['as' => 'admin.auth.login', 'uses' => 'Users\AuthController@login']);
            $router->get('admin/logout', ['as' => 'admin.auth.logout', 'uses' => 'Users\AuthController@logout']);

            $router->group(['middleware' => 'auth.admin'], function(Router $router) {

                $router->get('admin/logout', ['as' => 'admin.auth.logout', 'uses' => 'Users\AuthController@logout']);

                /**
                 * DashboardController
                 */
                $router->get('admin/dashboard', ['as' => 'admin.dashboard.index', 'uses' => 'Dashboard\AdminController@index']);
                $router->post('admin/featured', ['as' => 'admin.featured.update', 'uses' => 'Dashboard\AdminController@update']);
                $router->get('admin/featured/{model}/destroy', ['as' => 'admin.featured.destroy', 'uses' => 'Dashboard\AdminController@destroy']);

                $router->put('api/featured', ['as' => 'api.featured.update', 'uses' => 'Dashboard\ApiController@update']);

                /**
                 * FileController
                 */
                $router->get('admin/files', ['as' => 'admin.files.index', 'uses' => 'Files\AdminController@index']);

                /**
                 * UserController
                 */
                $router->get('admin/users', ['as' => 'admin.users.index', 'uses' => 'Users\AdminController@index']);
                $router->get('admin/users/create', ['as' => 'admin.users.create', 'uses' => 'Users\AdminController@create']);
                $router->post('admin/users', ['as' => 'admin.users.store', 'uses' => 'Users\AdminController@store']);
                $router->get('admin/users/{model}/edit', ['as' => 'admin.users.edit', 'uses' => 'Users\AdminController@edit']);
                $router->put('admin/users/{model}', ['as' => 'admin.users.update', 'uses' => 'Users\AdminController@update']);
                $router->get('admin/users/{model}/destroy', ['as' => 'admin.users.destroy', 'uses' => 'Users\AdminController@destroy']);

                /**
                 * GroupController
                 */
                $router->get('admin/groups', ['as' => 'admin.groups.index', 'uses' => 'Groups\AdminController@index']);
                $router->get('admin/groups/create', ['as' => 'admin.groups.create', 'uses' => 'Groups\AdminController@create']);
                $router->post('admin/groups', ['as' => 'admin.groups.store', 'uses' => 'Groups\AdminController@store']);
                $router->get('admin/groups/{model}/edit', ['as' => 'admin.groups.edit', 'uses' => 'Groups\AdminController@edit']);
                $router->put('admin/groups/{model}', ['as' => 'admin.groups.update', 'uses' => 'Groups\AdminController@update']);
                $router->get('admin/groups/{model}/destroy', ['as' => 'admin.groups.destroy', 'uses' => 'Groups\AdminController@destroy']);

                /**
                 * BlockController
                 */
                $router->get('admin/blocks', ['as' => 'admin.blocks.index', 'uses' => 'Blocks\AdminController@index']);
                $router->get('admin/blocks/create', ['as' => 'admin.blocks.create', 'uses' => 'Blocks\AdminController@create']);
                $router->post('admin/blocks', ['as' => 'admin.blocks.store', 'uses' => 'Blocks\AdminController@store']);
                $router->get('admin/blocks/{model}/edit', ['as' => 'admin.blocks.edit', 'uses' => 'Blocks\AdminController@edit']);
                $router->put('admin/blocks/{model}', ['as' => 'admin.blocks.update', 'uses' => 'Blocks\AdminController@update']);
                $router->get('admin/blocks/{model}/destroy', ['as' => 'admin.blocks.destroy', 'uses' => 'Blocks\AdminController@destroy']);

                /**
                 * DeliveryOptionController
                 */
                $router->get('admin/delivery-options', ['as' => 'admin.delivery-options.index', 'uses' => 'DeliveryOptions\AdminController@index']);
                $router->get('admin/delivery-options/create', ['as' => 'admin.delivery-options.create', 'uses' => 'DeliveryOptions\AdminController@create']);
                $router->post('admin/delivery-options', ['as' => 'admin.delivery-options.store', 'uses' => 'DeliveryOptions\AdminController@store']);
                $router->get('admin/delivery-options/{model}/edit', ['as' => 'admin.delivery-options.edit', 'uses' => 'DeliveryOptions\AdminController@edit']);
                $router->put('admin/delivery-options/{model}', ['as' => 'admin.delivery-options.update', 'uses' => 'DeliveryOptions\AdminController@update']);
                $router->get('admin/delivery-options/{model}/destroy', ['as' => 'admin.delivery-options.destroy', 'uses' => 'DeliveryOptions\AdminController@destroy']);

                /**
                 * OrderController
                 */
                $router->get('admin/orders', ['as' => 'admin.orders.index', 'uses' => 'Orders\AdminController@index']);
                $router->get('admin/orders/create', ['as' => 'admin.orders.create', 'uses' => 'Orders\AdminController@create']);
                $router->post('admin/orders', ['as' => 'admin.orders.store', 'uses' => 'Orders\AdminController@store']);
                $router->get('admin/orders/{model}/edit', ['as' => 'admin.orders.edit', 'uses' => 'Orders\AdminController@edit']);
                $router->put('admin/orders/{model}', ['as' => 'admin.orders.update', 'uses' => 'Orders\AdminController@update']);
                $router->get('admin/orders/{model}/destroy', ['as' => 'admin.orders.destroy', 'uses' => 'Orders\AdminController@destroy']);
                $router->get('admin/orders/{model}/dispatch', ['as' => 'admin.orders.dispatch', 'uses' => 'Orders\AdminController@dispatch']);
                $router->get('admin/orders/csv', ['as' => 'admin.orders.csv', 'uses' => 'Orders\AdminController@csv']);

                /**
                 * PageController
                 */
                $router->get('admin/content/{template}', ['as' => 'admin.pages.index', 'uses' => 'Pages\AdminController@index']);
                $router->get('admin/content/{template}/create', ['as' => 'admin.pages.create', 'uses' => 'Pages\AdminController@create']);
                $router->post('admin/content/{template}', ['as' => 'admin.pages.store', 'uses' => 'Pages\AdminController@store']);
                $router->get('admin/content/{template}/{id}/edit', ['as' => 'admin.pages.edit', 'uses' => 'Pages\AdminController@edit']);
                $router->put('admin/content/{template}/{id}', ['as' => 'admin.pages.update', 'uses' => 'Pages\AdminController@update']);
                $router->get('admin/content/{template}/{id}/destroy', ['as' => 'admin.pages.destroy', 'uses' => 'Pages\AdminController@destroy']);

                /**
                 * SettingsController
                 */
                $router->get('admin/settings', ['as' => 'admin.settings.edit', 'uses' => 'Settings\AdminController@edit']);
                $router->put('admin/settings', ['as' => 'admin.settings.update', 'uses' => 'Settings\AdminController@update']);

                /**
                 * FieldController
                 */
                $router->get('api/fields/{template}/{page?}', ['as' => 'api.fields.index', 'uses' => 'Fields\ApiController@index']);

                /**
                 * TemplateController
                 */
                $router->get('api/templates/{model}', ['as' => 'api.templates.index', 'uses' => 'Templates\ApiController@index']);

            });
        });
        
        $router->get('sitemap.xml', 'Panda\Http\Controllers\Sitemap\PublicController@index');

        $router->get('{uri}', 'App\Http\Controllers\ContentController@index')->where('uri', '(.*)');
    }
}
