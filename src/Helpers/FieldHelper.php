<?php

namespace Panda\Helpers;

use Panda\Models\Field;
use Panda\Models\FieldType;

class FieldHelper
{
    public static function generate($id, $name, $label, $order, $size, $validation, $fieldTypeId, $value)
    {
        $field = new Field();

        $field->fill([
            'id' => $id,
            'name' => $name,
            'label' => $label,
            'order' => $order,
            'size' => $size,
            'validation' => $validation,
            'field_type_id' => $fieldTypeId
        ]);

        $fieldType = FieldType::find($fieldTypeId);

        $class = new $fieldType->class($field, $value);

        return $class->render();
    }
}