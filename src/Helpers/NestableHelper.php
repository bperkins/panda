<?php

namespace Panda\Helpers;

use App;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as BaseCollection;

class NestableHelper extends Collection
{
    private $total;
    private $parentColumn;

    public function __construct($items)
    {
        parent::__construct($items);
        $this->parentColumn = 'parent_id';
        $this->total = count($items);
    }

    public function nest()
    {
        $parentColumn = $this->parentColumn;
        if(!$parentColumn) {
            return $this;
        }

        $this->items = $this->getDictionary();

        $keysToDelete = array();

        $this->each(function($item) {
            if(!$item->items) {
                $item->items = App::make('Illuminate\Support\Collection');
            }
        });

        foreach($this->items as $key => $item) {
            if($item->$parentColumn && isset($this->items[$item->$parentColumn])) {
                $this->items[$item->$parentColumn]->items->push($item);
                $keysToDelete[] = $item->id;
            }
        }

        $this->items = array_values(array_except($this->items, $keysToDelete));

        return $this;
    }

    public function nestList($column = 'title', BaseCollection $collection = null, $level = 0, array &$flattened = [], $indentChars = '- ')
    {
        $collection = $collection ?: $this->nest();

        foreach($collection as $item) {
            $item->title = str_repeat($indentChars, $level).$item->$column;
            $flattened[$item->id] = $item;
            if($item->items) {
                $this->nestList($column, $item->items, $level + 1, $flattened, $indentChars);
            }
        }

        return $flattened;
    }

    public function total()
    {
        return $this->total;
    }
}