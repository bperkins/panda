$(function () {
    $('.featured select').on('change', function () {
        var self = $(this);
        console.log(self.val());
        if (self.val() > 0) {
            self.parents('form').submit();
        }
    });
    $('.featured ul').sortable({
        stop: function (event, ui) {
            var items = $('.featured__item'),
                order = [];
            
            items.each(function (index, elem) {
                order.push($(elem).data('id'));
            });

            $.ajax({
                url: '/api/featured',
                method: 'put',
                data: {
                    'items': order
                }
            })
        }
    });
});
