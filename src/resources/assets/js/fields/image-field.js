$(function() {
    $('.btn-elfinder').on('click', function(e) {
        e.preventDefault();
        $('<div id="editor" />').dialogelfinder({
            url : '/admin/elfinder/connector',
            getFileCallback: function(file) {
                $parent = $(e.target).parents('.image-field');
                $imagesElement = $parent.find('.images');
                $editor = $('#editor');
                $editor.dialogelfinder('close');
                $editor.closest('.elfinder').val(file.path);
                $editor.remove();
                $element = $parent.find('input[name="' + $parent.data('id') + '"]');
                $images = [];
                if($element.val().length > 0) {
                    $images = JSON.parse($element.val());
                }
                $imagesElement.append('<div class="col-md-2"><div class="image-field__container"><div class="image-field__image" style="background-image: url(/' + file.path + ');"></div><a href="#" data-id="' + ($images.length) + '" class="image-field__remove">X</a></div></div>');
                appendImage(file, $parent);
            }
        });
    });
    $('body').on('click', '.image-field__remove', function(e) {
        e.preventDefault();
        $parent = $(this).parents('.image-field');
        $element = $parent.find('input[name="' + $parent.data('id') + '"]');
        $images = JSON.parse($element.val());
        $images.splice($(this).data('id'), 1);
        $imageElements = $(this).parents('.images').find('.image-field__remove');
        for($i = 0; $i < $imageElements.length; $i++) {
            $($imageElements[$i]).data('id', $i);
        }
        console.log($imageElements);
        $json = JSON.stringify($images);
        $element.val($json);
        $(this).parents('.col-md-2').remove();
        if($images.length === 0) {
            $parent.find('hr').hide();
        }
    });
    function appendImage(file, parent) {
        $element = parent.find('input[name="' + parent.data('id') + '"]');
        $images = [];
        if($element.val().length > 0) {
            $images = JSON.parse($element.val());
        }
        $image = {};
        $image.url = '/' + file.path;
        $images.push($image);
        console.log($images);
        $json = JSON.stringify($images);
        $element.val($json);
        parent.find('hr').show();
    }
});
