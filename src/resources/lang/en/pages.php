<?php

return [
    'index' => 'Pages',
    'create' => 'Add New Page',
    'edit' => 'Edit Page'
];