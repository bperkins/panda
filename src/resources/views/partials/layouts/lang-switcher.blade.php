<div class="btn-group pull-right">
    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span id="active-locale">English <span class="caret"></span></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="#" class="btn-lang-js active" data-locale="en">English</a></li>
    </ul>
</div>