<div id="sidebar" class="col-xs-6 col-sm-3 col-md-2 sidebar sidebar-offcanvas" role="navigation">
    <ul class="sidebar-menu">
        <li class="sidebar-panel">
            <ul class="nav nav-sidebar panel-collapse collapse in" id="dashboard">
                <li class="clearfix {{ request()->is('admin/dashboard*') ? 'active' : '' }}">
                    <a href="{{ route('admin.dashboard.index') }}">
                        <i class="icon fa fa-fw fa-dashboard"></i>
                        <div>Dashboard</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="sidebar-panel">
            <div class="sidebar-title">Content</div>
            <ul class="nav nav-sidebar panel-collapse collapse in" id="content">
                @foreach($templates as $template)
                    <li class="clearfix {{ request()->is('admin/content/'.$template->name.'*') ? 'active' : '' }}">
                        <a href="{{ url('admin/content/'.$template->name) }}">
                            <i class="icon fa fa-fw {{ !empty($template->icon) ? $template->icon : 'fa-file' }}"></i>
                            <div>{{ $template->label }}</div>
                        </a>
                    </li>
                @endforeach
                <li class="clearfix {{ request()->is('admin/blocks*') ? 'active' : '' }}">
                    <a href="{{ route('admin.blocks.index') }}">
                        <i class="icon fa fa-fw fa-list-alt"></i>
                        <div>Blocks</div>
                    </a>
                </li>
            </ul>
        </li>
        @if (config('panda.modules.commerce'))
            <li class="sidebar-panel">
                <div class="sidebar-title">Commerce</div>
                <ul class="nav nav-sidebar panel-collapse collapse in" id="commerce">
                    <!--<li class="clearfix {{ request()->is('admin/delivery-options*') ? 'active' : '' }}">
                        <a href="{{ action('DeliveryOptions\AdminController@index') }}">
                            <i class="icon fa fa-fw fa-truck"></i>
                            <div>Delivery Options</div>
                        </a>
                    </li>-->
                    <li class="clearfix {{ request()->is('admin/orders*') ? 'active' : '' }}">
                        <a href="{{ action('Orders\AdminController@index') }}">
                            <i class="icon fa fa-fw fa-shopping-basket"></i>
                            <div>Orders</div>
                        </a>
                    </li>
                </ul>
            </li>
        @endif
        <li class="sidebar-panel">
            <div class="sidebar-title">Media</div>
            <ul class="nav nav-sidebar panel-collapse collapse in" id="media">
                <li class="clearfix {{ request()->is('admin/files*') ? 'active' : '' }}">
                    <a href="{{ route('admin.files.index') }}">
                        <i class="icon fa fa-fw fa-file-photo-o"></i>
                        <div>Files</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="sidebar-panel">
            <div class="sidebar-title">Users and groups</div>
            <ul class="nav nav-sidebar panel-collapse collapse in" id="users">
                <li class="clearfix {{ request()->is('admin/users*') ? 'active' : '' }}">
                    <a href="{{ route('admin.users.index') }}">
                        <i class="icon fa fa-fw fa-user"></i>
                        <div>Users</div>
                    </a>
                </li>
                <li class="clearfix {{ request()->is('admin/groups*') ? 'active' : '' }}">
                    <a href="{{ route('admin.groups.index') }}">
                        <i class="icon fa fa-fw fa-users"></i>
                        <div>Groups</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="sidebar-panel">
            <div class="sidebar-title">Settings</div>
            <ul class="nav nav-sidebar panel-collapse collapse in" id="settings">
                <li class="clearfix {{ request()->is('admin/settings*') ? 'active' : '' }}">
                    <a href="{{ route('admin.settings.edit') }}">
                        <i class="icon fa fa-fw fa-cog"></i>
                        <div>Site Settings</div>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>
