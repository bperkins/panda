<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header"><a href="{{ route('admin.dashboard.index') }}" class="navbar-brand">{{ config('panda.name') }}</a></div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ route('admin.auth.logout') }}">Logout</a></li>
        </ul>
    </div>
</nav>
