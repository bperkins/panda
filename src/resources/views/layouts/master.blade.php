<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>{{ config('panda.name') }}</title>
        <link rel="stylesheet" href="{{ asset('css/panda.css') }}">
        @yield('css')
        <link rel="stylesheet" href="{{ asset('components/jquery-ui/jquery-ui.min.css') }}">
        <link rel="stylesheet" href="{{ asset('packages/barryvdh/elfinder/css/elfinder.min.css') }}">
        <link rel="stylesheet" href="{{ asset('packages/barryvdh/elfinder/css/theme.css') }}">
    </head>
    <body class="@yield('class')">
        @yield('scaffolding')
        <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('js/panda.min.js') }}"></script>
        <script src="{{ asset('components/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('packages/barryvdh/elfinder/js/elfinder.min.js') }}"></script>
        <script type="text/javascript">
            $(function() {
                $('.elfinder').elfinder({
                    url: '{{ route('elfinder.connector') }}'
                });
            });
        </script>
    </body>
</html>
