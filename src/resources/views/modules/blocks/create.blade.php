@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::blocks.create')</h1>
    <form action="{{ url('admin/blocks') }}" method="POST" role="form">
        {{ csrf_field() }}
        @include('panda::modules.blocks.form')
    </form>
@endsection