@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::blocks.edit')</h1>
    <form action="{{ url('admin/blocks/'.$model->id) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="id" value="{{ $model->id }}">
        {{ csrf_field() }}
        @include('panda::modules.blocks.form')
    </form>
@endsection