@section('js')
    <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
@endsection

@include('panda::partials.form.actions')
<input type="hidden" id="id" name="id" value="{{ $model->id }}">
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-content" data-target="#tab-content" data-toggle="tab">Content</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade in active" id="tab-content">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title" value="{{ old('title') ? old('title') : $model->title }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="template_id">Template</label>
                    <select name="template_id" class="form-control" id="template_id">
                        @foreach($templates as $id => $name)
                            <option value="{{ $id }}" @if(old('template_id') && $id == old('template_id')) selected @elseif($id == $model->template_id) selected @endif>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div id="fields">{!! $form ? $form->render($model) : '' !!}</div>
        </div>
    </div>
</div>
