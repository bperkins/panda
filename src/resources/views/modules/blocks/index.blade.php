@extends('panda::layouts.scaffolding')

@section('body')
    <h1>Blocks</h1>
    <div class="action-bar pull-left">
        <form class="form-inline" method="get">
            <a href="{{ route('admin.blocks.create') }}" class="btn btn-default">Add New</a>
            <input type="text" name="search" id="search" class="form-control" placeholder="Search..." value="{{ $search }}">
        </form>
    </div>
    @include ('panda::partials.layouts.pagination')
    <div class="clearfix"></div>
    <div class="panel panel-default">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-inverse">
                    <tr>
                        <th width="80%">Title</th>
                        <th width="20%">Published</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($models as $model)
                        <tr>
                            <td>
                                <div>
                                    <a href="{{ route('admin.blocks.edit', $model->id) }}">{{ $model->title }}</a>
                                </div>
                                <div class="actions"><span><a href="{{ route('admin.blocks.edit', $model->id) }}">Edit</a> | Delete</span></div>
                            </td>
                            <td>
                                <div>
                                    <span>{{ $model->updated_at->format('d-m-y H:i') }}</span>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
