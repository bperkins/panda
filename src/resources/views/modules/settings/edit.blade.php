@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::settings.edit')</h1>
    <form action="{{ route('admin.settings.update') }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        @include('panda::modules.settings.form')
    </form>
@endsection
