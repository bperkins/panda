<div class="btn-toolbar">
    <button type="submit" class="btn btn-default">Save</button>
</div>
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-general" data-target="#tab-general" data-toggle="tab">General</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab-general">
        <div class="row">
           {!! $fields !!}
        </div>
    </div>
</div>
