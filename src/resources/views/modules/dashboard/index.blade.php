@extends('panda::layouts.scaffolding')
@section('body')
    <div class="dashboard">
        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default stat">
                    <div class="panel-body">
                        <h2>{{ $orderCount }}</h2>
                        <p>Orders placed</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="{{ $dispatchedCount }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ $dispatchedCount }}%;">{{ $dispatchedCount }}%</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default stat">
                    <div class="panel-body">
                        <h2>{{ $productCount }}</h2>
                        <p>Products created</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default stat">
                    <div class="panel-body">
                        <h2>{{ $userCount }}</h2>
                        <p>Users created</p>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (config('panda.modules.commerce'))
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default featured">
                        <div class="panel-heading">
                            <i class="fa fa-star"></i> <span>Featured</span>
                            <spam class="pull-right">({{ $featured->count() }})</spam>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                @if ($featured->count() < 4)
                                    <div class="col-sm-12">
                                        <form method="post" action="{{ route('admin.featured.update') }}">
                                            {{ csrf_field() }}
                                            <select name="featured" class="form-control">
                                                <option value="" selected disabled>-- Add Product --</option>
                                                @foreach ($products as $product)
                                                    <option value="{{ $product->id }}">{{ $product->title }} ({{ $product->fields->lot_number }})</option>
                                                @endforeach
                                            </select>
                                        </form>
                                        <hr>
                                    </div>
                                @endif
                                <ul>
                                    @foreach ($featured as $product)
                                        <li data-id="{{ $product->id }}" class="col-sm-3 featured__item">
                                            <div class="product" style="background-image: url('{{ isset($product->fields->images[0]->url) ? $product->fields->images[0]->url : null }}');">
                                                <div class="remove">
                                                    <a href="{{ route('admin.featured.destroy', $product->id) }}"><i class="fa fa-trash-o"></i></a>
                                                </div>
                                                <div class="name">
                                                    <span><a href="{{ route('admin.pages.edit', ['products', $product->id]) }}">{{ $product->title }}</a></span>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-shopping-basket"></i> <span>Alerts</span>
                        <span class="pull-right">({{ count($orders) }})</span>
                    </div>
                    @if (count($orders) > 0)
                        <div class="panel-body">
                            <span>Below are orders which still need reviewing and dispatching. Once dispatched the order will be removed from the list.</span>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="thead-inverse">
                                    <tr>
                                        <th width="40%">Reference</th>
                                        <th width="10%">Paid</th>
                                        <th width="10%">Sent</th>
                                        <th width="20%">Total</th>
                                        <th width="20%">Published</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($orders as $order)
                                        <tr>
                                            <td>
                                                <div>
                                                    <a href="{{ url('admin/orders/'.$order->id.'/edit') }}">{{ $order->reference }}</a>
                                                </div>
                                                <div class="actions"><span><a href="{{ action('Orders\AdminController@dispatch', $order) }}">Dispatch</a> | <a href="{{ url('admin/orders/'.$order->id.'/edit') }}">Edit</a> | <a href="{{ url('admin/orders/'.$order->id.'/destroy') }}">Delete</a></span></div>
                                            </td>
                                            <td>
                                                <div>
                                                    {{ $order->paid ? 'Yes' : 'No' }}
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    {{ $order->sent ? 'Yes' : 'No' }}
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    &pound;{{ number_format($order->price, 2) }}
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    <span>{{ $order->updated_at->format('d-m-y H:i') }}</span>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
