@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::delivery-options.edit')</h1>
    <form action="{{ action('DeliveryOptions\AdminController@update', $model->id) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        @include('panda::modules.delivery-options.form')
    </form>
@endsection