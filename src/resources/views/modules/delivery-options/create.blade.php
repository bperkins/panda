@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::delivery-options.create')</h1>
    <form action="{{ action('DeliveryOptions\AdminController@store') }}" method="POST">
        {{ csrf_field() }}
        @include('panda::modules.delivery-options.form')
    </form>
@endsection
