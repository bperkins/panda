@extends('panda::layouts.scaffolding')

@section('body')
    <h1>Delivery Options</h1>
    <div class="action-bar pull-left">
        <form class="form-inline" method="get">
            <a href="{{ url('admin/delivery-options/create') }}" class="btn btn-default">Add New</a>
            <input type="text" name="search" id="search" class="form-control" placeholder="Search..." value="{{ $search }}">
        </form>
    </div>
    @include ('panda::partials.layouts.pagination')
    <div class="clearfix"></div>
    <div class="panel panel-default">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-inverse">
                    <tr>
                        <th width="80%">Title</th>
                        <th width="20%">Published</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($models as $model)
                        <tr>
                            <td>
                                <div>
                                    <a href="{{ url('admin/delivery-options/'.$model->id.'/edit') }}">{{ $model->title }}</a>
                                </div>
                                <div class="actions"><span><a href="{{ url('admin/delivery-options/'.$model->id.'/edit') }}">Edit</a> | <a href="{{ url('admin/delivery-options/'.$model->id.'/destroy') }}">Delete</a></span></div>
                            </td>
                            <td>
                                <div>
                                    <span>{{ $model->updated_at->format('d-m-y H:i') }}</span>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection