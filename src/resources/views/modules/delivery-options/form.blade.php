<div class="btn-toolbar">
    <button type="submit" value="true" id="edit" name="exit" class="btn btn-primary">Save and exit</button>
    <button type="submit" class="btn btn-default">Save</button>
</div>
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-content" data-target="#tab-content" data-toggle="tab">Content</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab-content">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title" value="{{ old('title') ? old('title') : $model->title }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="price">Price</label>
                    <input type="text" name="price" class="form-control" id="price" value="{{ old('price') ? old('price') : $model->price }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="description">Description</label>
                    <textarea name="description" class="form-control" id="description">{{ old('description') ? old('description') : $model->description }}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
