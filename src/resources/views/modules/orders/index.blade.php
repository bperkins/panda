@extends('panda::layouts.scaffolding')

@section('body')
    <h1>Orders</h1>
    <div class="action-bar pull-left">
        <form class="form-inline" method="get">
            <a href="{{ url('admin/orders/create') }}" class="btn btn-default">Add New</a>
            <input type="text" name="search" id="search" class="form-control" placeholder="Search..." value="{{ $search }}">
        </form>
    </div>
    <div class="action-bar pull-right">
        <form class="form-inline">
            <a href="{{ route('admin.orders.csv') }}" class="btn btn-default">Download CSV</a>
        </form>
    </div>
    @include ('panda::partials.layouts.pagination')
    <div class="clearfix"></div>
    <div class="panel panel-default">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead class="thead-inverse">
                    <tr>
                        <th width="40%">Reference</th>
                        <th width="10%">Paid</th>
                        <th width="10%">Sent</th>
                        <th width="20%">Total</th>
                        <th width="20%">Published</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($models as $model)
                        <tr>
                            <td>
                                <div>
                                    <a href="{{ url('admin/orders/'.$model->id.'/edit') }}">{{ $model->reference }}</a>
                                </div>
                                <div class="actions"><span><a href="{{ url('admin/orders/'.$model->id.'/edit') }}">Edit</a> | <a href="{{ url('admin/orders/'.$model->id.'/destroy') }}">Delete</a></span></div>
                            </td>
                            <td>
                                <div>
                                    {{ $model->paid ? 'Yes' : 'No' }}
                                </div>
                            </td>
                            <td>
                                <div>
                                    {{ $model->sent ? 'Yes' : 'No' }}
                                </div>
                            </td>
                            <td>
                                <div>
                                    &pound;{{ number_format($model->price, 2) }}
                                </div>
                            </td>
                            <td>
                                <div>
                                    <span>{{ $model->updated_at->format('d-m-y H:i') }}</span>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection