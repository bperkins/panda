@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::orders.edit')</h1>
    <form action="{{ action('Orders\AdminController@update', $model->id) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        @include('panda::modules.orders.form')
    </form>
@endsection