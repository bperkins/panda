@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::orders.create')</h1>
    <form action="{{ action('Orders\AdminController@store') }}" method="POST">
        {{ csrf_field() }}
        @include('panda::modules.orders.form')
    </form>
@endsection
