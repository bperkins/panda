<div class="btn-toolbar">
    <button type="submit" value="true" id="edit" name="exit" class="btn btn-primary">Save and exit</button>
    <button type="submit" class="btn btn-default">Save</button>
</div>
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-content" data-target="#tab-content" data-toggle="tab">Content</a>
    </li>
    <li>
        <a href="#tab-address" data-target="#tab-address" data-toggle="tab">Address</a>
    </li>
    <li>
        <a href="#tab-products" data-target="#tab-products" data-toggle="tab">Products</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab-content">
        <div class="row">
            <div class="col-md-12">
                @if ($model->reference)
                    <div class="form-group">
                        <label class="control-label" for="reference">Reference</label>
                        <input type="text" name="reference" class="form-control" id="reference" value="{{ old('reference') ? old('reference') : $model->reference }}" disabled>
                    </div>
                @endif
                <div class="form-group">
                    <label class="control-label" for="paid">Paid</label>
                    <select name="paid" id="paid" class="form-control">
                        <?php $options = [0 => 'No', 1 => 'Yes']; ?>
                        @foreach ($options as $key => $value)
                            <option value="{{ $key }}" {{ old('paid') && old('paid') === 1 ? 'selected' : $model->paid ? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label" for="sent">Sent</label>
                    <select name="sent" id="sent" class="form-control">
                        <?php $options = [0 => 'No', 1 => 'Yes']; ?>
                        @foreach ($options as $key => $value)
                            <option value="{{ $key }}" {{ old('sent') && old('sent') === 1 ? 'selected' : $model->sent ? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label" for="first_name">First Name</label>
                    <input type="text" name="first_name" class="form-control" id="first_name" value="{{ old('first_name') ? old('first_name') : $model->first_name }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="last_name">Last Name</label>
                    <input type="text" name="last_name" class="form-control" id="last_name" value="{{ old('last_name') ? old('last_name') : $model->last_name }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" value="{{ old('email') ? old('email') : $model->email }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="telephone">Telephone</label>
                    <input type="number" name="telephone" class="form-control" id="telephone" value="{{ old('telephone') ? old('telephone') : $model->telephone }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="price">Price</label>
                    <input type="text" name="price" class="form-control" id="price" value="{{ old('price') ? old('price') : $model->price }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="delivery_price">Delivery Price</label>
                    <input type="text" name="delivery_price" class="form-control" id="delivery_price" value="{{ old('delivery_price') ? old('delivery_price') : $model->delivery_price }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="delivery_option_id">Delivery Option</label>
                    <select name="delivery_option_id" class="form-control" id="delivery_option_id">
                        @foreach ($deliveryOptions as $id => $title)
                            <option value="{{ $id }}" {{ old('delivery_option_id') ? old('delivery_option_id') === $id ? 'selected' : '' : $model->delivery_option_id === $id ? 'selected' : '' }}>{{ $title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label" for="delivery_region_id">Delivery Region</label>
                    <select name="delivery_region_id" class="form-control" id="delivery_region_id">
                        @foreach ($deliveryRegions as $id => $title)
                            <option value="{{ $id }}" {{ old('delivery_region_id') ? old('delivery_region_id') === $id ? 'selected' : '' : $model->delivery_region_id === $id ? 'selected' : '' }}>{{ $title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="tab-address">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" for="address_line_1">Address line 1</label>
                    <input type="text" name="address[address_line_1]" class="form-control" id="address_line_1" value="{{ old('address.address_line_1') ? old('address.address_line_1') : $model->address->address_line_1 }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="address_line_2">Address line 2</label>
                    <input type="text" name="address[address_line_2]" class="form-control" id="address_line_2" value="{{ old('address.address_line_2') ? old('address.address_line_2') : $model->address->address_line_2 }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="city">City</label>
                    <input type="text" name="address[city]" class="form-control" id="city" value="{{ old('address.city') ? old('address.city') : $model->address->city }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="county">County</label>
                    <input type="text" name="address[county]" class="form-control" id="county" value="{{ old('address.county') ? old('address.county') : $model->address->county }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="country">Country</label>
                    <input type="text" name="address[country]" class="form-control" id="country" value="{{ old('address.country') ? old('address.country') : $model->address->country }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="postcode">Postcode</label>
                    <input type="text" name="address[postcode]" class="form-control" id="postcode" value="{{ old('address.postcode') ? old('address.postcode') : $model->address->postcode }}">
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="tab-products">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <table class="table">
                        <thead>
                            <tr>
                                <th width="15%">Lot #</th>
                                <th width="70%">Item</th>
                                <th width="15%">Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($model->items->count() === 0)
                                <tr>
                                    <td colspan="3">No items found.</td>
                                </tr>
                            @endif
                            @foreach ($model->items as $item)
                                <tr>
                                    <td>{{ $item->lot_number }}</td>
                                    <td>{{ $item->item }}</td>
                                    <td>&pound;{{ number_format($item->price, 2) }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
