@extends('panda::layouts.master')

@section('class', 'auth')

@section('scaffolding')
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-lock"></i>
                        <span>{{ config('panda.name') }}</span>
                    </div>
                    <div class="panel-body">
                        <form action="{{ route('admin.auth.login') }}" method="post" class="text-center">
                            <div class="form-group">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Login</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection