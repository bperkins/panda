@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::users.create')</h1>
    <form action="{{ url('admin/users') }}" method="POST" role="form">
        {{ csrf_field() }}
        @include('panda::modules.users.form')
    </form>
@endsection