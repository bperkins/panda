@include('panda::partials.form.actions')
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-content" data-target="#tab-content" data-toggle="tab">Content</a>
    </li>
    <li>
        <a href="#tab-address" data-target="#tab-address" data-toggle="tab">Address</a>
    </li>
    <li>
        <a href="#tab-permissions" data-target="#tab-permissions" data-toggle="tab">Permissions</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab-content">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="first_name">First name</label>
                    <input type="text" name="first_name" id="first_name" class="form-control" value="{{ old('first_name') ? old('first_name') : $model->first_name }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="last_name">Last name</label>
                    <input type="text" name="last_name" id="last_name" class="form-control" value="{{ old('last_name') ? old('last_name') : $model->last_name }}">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control" value="{{ old('email') ? old('email') : $model->email }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="password">Password</label>
                    <input type="password" name="password" id="password" class="form-control">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="password_confirmation">Password confirmation</label>
                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="tab-address">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" for="address_line_1">Address line 1</label>
                    <input type="text" name="address[address_line_1]" class="form-control" id="address_line_1" value="{{ old('address.address_line_1') ? old('address.address_line_1') : $model->address->address_line_1 }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="address_line_2">Address line 2</label>
                    <input type="text" name="address[address_line_2]" class="form-control" id="address_line_2" value="{{ old('address.address_line_2') ? old('address.address_line_2') : $model->address->address_line_2 }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="city">City</label>
                    <input type="text" name="address[city]" class="form-control" id="city" value="{{ old('address.city') ? old('address.city') : $model->address->city }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="county">County</label>
                    <input type="text" name="address[county]" class="form-control" id="county" value="{{ old('address.county') ? old('address.county') : $model->address->county }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="country">Country</label>
                    <input type="text" name="address[country]" class="form-control" id="country" value="{{ old('address.country') ? old('address.country') : $model->address->country }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="postcode">Postcode</label>
                    <input type="text" name="address[postcode]" class="form-control" id="postcode" value="{{ old('address.postcode') ? old('address.postcode') : $model->address->postcode }}">
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="tab-permissions">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" for="groups[]">Groups</label>
                    <select name="groups[]" id="groups" class="form-control" multiple="multiple">
                        @foreach ($groups as $id => $name)
                            <option value="{{ $id }}" @if(old('groups') ? in_array($id, old('groups')) : $model->groups && $model->groups->contains('id', $id)){{ 'selected' }}@endif>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>