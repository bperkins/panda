@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::global.create') {{ rtrim($template->label, 's') }}</h1>
    <form action="{{ url('admin/content/'.$template->name) }}" method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="template_id" value="{{ $template->id }}">
        @include('panda::modules.pages.form')
    </form>
@endsection