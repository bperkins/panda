@section('js')
    <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
@endsection

<div class="btn-toolbar">
    <button type="submit" value="true" id="edit" name="exit" class="btn btn-primary">Save and exit</button>
    <button type="submit" class="btn btn-default">Save</button>
</div>
<input type="hidden" id="id" name="id" value="{{ $model->id }}">
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-content" data-target="#tab-content" data-toggle="tab">Content</a>
    </li>
    <li>
        <a href="#tab-meta" data-target="#tab-meta" data-toggle="tab">Meta</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab-content">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title" value="{{ old('title') ? old('title') : $model->title }}">
                </div>
            </div>
            @if($template->has_parent && !$template->parents->count() && $parents)
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="parent_id">Parent</label>
                        <select name="parent_id" class="form-control" id="parent_id">
                            @foreach($parents as $pageId => $page)
                                <option value="{{ $pageId }}" @if((old('parent_id') && $pageId == old('parent_id')) || (!old('parent_id') && $pageId == $model->parent_id)){{ 'selected' }}@endif>{{ $page->title }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            @endif
            @if($template->has_uri)
                <div class="col-md-12">
                    <div class="form-group">
                        <label class="control-label" for="slug">URL</label>
                        <div class="input-group">
                            <span class="input-group-addon">/</span>
                            <input type="text" name="slug" class="form-control" id="slug" value="{{ old('slug') ? old('slug') : $model->slug }}" data-slug="title">
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" for="status">Publish</label>
                    <select name="status" class="form-control" id="status">
                        <?php $options = [1 => 'Yes', 0 => 'No']; ?>
                        @foreach ($options as $key => $value)
                            <option value="{{ $key }}" {{ old('status') && old('status') === $key ? 'selected' : $model->status === $key ? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div id="fields">{!! $form ? $form->render($model) : '' !!}</div>
        </div>
    </div>
    <div class="tab-pane" id="tab-meta">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" for="meta_title">Meta Title</label>
                    <input type="text" name="meta_title" class="form-control" id="meta_title" value="{{ old('meta_title') ? old('meta_title') : $model->meta_title }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="meta_description">Meta Description</label>
                    <textarea name="meta_description" class="form-control" id="meta_description">{{ old('meta_title') ? old('meta_title') : $model->meta_description }}</textarea>
                </div>
                <div class="form-group">
                    <label class="control-label" for="meta_keywords">Meta Keywords</label>
                    <textarea name="meta_keywords" class="form-control" id="meta_keywords">{{ old('meta_keywords') ? old('meta_keywords') : $model->meta_keywords }}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>