@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::global.edit') {{ rtrim($template->label, 's') }}</h1>
    <form action="{{ url('admin/content/'.$template->name.'/'.$model->id) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="id" value="{{ $model->id }}">
        <input type="hidden" name="template_id" value="{{ $model->template_id }}">
        {{ csrf_field() }}
        @include('panda::modules.pages.form')
    </form>
@endsection