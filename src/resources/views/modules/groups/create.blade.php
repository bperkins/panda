@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::groups.create')</h1>
    <form action="{{ url('admin/groups') }}" method="POST" role="form">
        {{ csrf_field() }}
        @include('panda::modules.groups.form')
    </form>
@endsection