@include('panda::partials.form.actions')
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-content" data-target="#tab-content" data-toggle="tab">Content</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group">
                    <label class="control-label" for="name">Name</label>
                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name') ? old('name') : $model->name }}">
                </div>
            </div>
        </div>
    </div>
</div>