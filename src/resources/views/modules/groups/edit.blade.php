@extends('panda::layouts.scaffolding')

@section('body')
    <h1>@lang('panda::groups.edit')</h1>
    <form action="{{ url('admin/groups/'.$model->id) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        <input type="hidden" name="id" value="{{ $model->id }}">
        {{ csrf_field() }}
        @include('panda::modules.groups.form')
    </form>
@endsection