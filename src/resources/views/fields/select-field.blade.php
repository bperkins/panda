<div class="col-md-{{ $field->size }}">
    <div class="form-group">
        <label class="control-label" for="{{ $name }}">{{ $field->label }}</label>
        <select name="{{ $name }}" class="form-control" id="{{ $name }}" @if($multiple){{ 'multiple' }}@endif>
            @foreach($data as $key => $option)
                <option value="{{ $key }}" @if($key == $value || (is_array(json_decode($value)) && in_array($key, json_decode($value)))){{ 'selected' }}@endif>{{ $option }}</option>
            @endforeach
        </select>
    </div>
</div>
