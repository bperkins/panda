<div class="col-md-{{ $field->size }}">
    <div class="form-group">
        <label class="control-label" for="{{ $name }}">{{ $field->label }}</label>
        <textarea name="{{ $name }}" class="form-control ckeditor" id="{{ $name }}">{{ $value }}</textarea>
    </div>
</div>
