<div class="col-md-{{ $field->size }}">
    <div class="image-field" data-id="fields[{{ $field->id }}]">
        <input type="hidden" id="{{ $name }}" name="{{ $name }}" value="{{ $value }}">
        <div class="form-group">
            <label class="control-label" for="{{ $name }}">{{ $field->label }}</label>
            <div class="well">
                <div class="row">
                    <div class="col-md-12">
                        <a href="#" class="btn btn-default btn-elfinder">Add New Image</a>
                        <hr style="{{ count($images) > 0 ? 'display: block;' : '' }}">
                    </div>
                </div>
                <div class="row images">
                    @foreach($images as $key => $image)
                        <div class="col-md-2">
                            <div class="image-field__container">
                                <div class="image-field__image" style="background-image: url('{{ $image->url }}');"></div>
                                <a href="#" data-id="{{ $key }}" class="image-field__remove">X</a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
