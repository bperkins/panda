<div class="col-md-{{ $field->size }}">
    <div class="form-group">
        <label class="control-label" for="{{ $name }}">{{ $field->label }}</label>
        <input type="text" name="{{ $name }}" class="form-control" id="{{ $name }}" value="{{ $value }}">
    </div>
</div>
