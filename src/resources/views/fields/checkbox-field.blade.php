<div class="col-md-{{ $field->size }}">
    <div class="form-group">
        <label class="control-label" for="{{ $name }}">
            <input type="checkbox" name="{{ $name }}" id="{{ $name }}" value="1" @if($value){{ 'checked' }}@endif>
            <span>{{ $field->label }}</span>
        </label>
    </div>
</div>
