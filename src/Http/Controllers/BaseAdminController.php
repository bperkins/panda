<?php

namespace Panda\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Routing\Controller;
use Illuminate\Database\Eloquent\Model;
use Panda\Repositories\Eloquent\BaseRepository;

class BaseAdminController extends Controller
{
    /**
     * The global repository for the child controller.
     *
     * @var BaseRepository $repository
     */
    protected $repository;

    public function __construct($repository = null)
    {
        // Force the middleware for security.
        $this->middleware(['admin']);

        // Set the global repository.
        $this->repository = $repository;
    }

    /**
     * Redirects the user to an index or edit page via the model.
     *
     * @param Request $request
     * @param Model $model
     * @return Redirector
     */
    protected function redirect($request, $model)
    {
        // Get the correct redirect string from the model.
        $redirectUrl = $request->get('exit') ? $model->indexUrl() : $model->editUrl();

        return redirect($redirectUrl);
    }
}
