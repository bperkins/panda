<?php

namespace Panda\Http\Controllers\Templates;

use Panda\Models\TemplateType;
use Panda\Repositories\TemplateRepository;
use Panda\Http\Controllers\BaseApiController;

class ApiController extends BaseApiController
{
	public function __construct(TemplateRepository $repository)
	{
		parent::__construct($repository);
	}

	public function index($model)
	{
		$models = $this->repository->allBy('template_type_id', $model, [], true);

		return response()
			->json($models, 200);
	}
}