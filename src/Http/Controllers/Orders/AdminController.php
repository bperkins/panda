<?php

namespace Panda\Http\Controllers\Orders;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Panda\Http\Controllers\BaseAdminController;
use Panda\Models\Address;
use Panda\Models\Order;
use Panda\Repositories\DeliveryOptionRepository;
use Panda\Repositories\DeliveryRegionRepository;
use Panda\Repositories\OrderRepository;
use Panda\Repositories\PageRepository;
use Mail;

class AdminController extends BaseAdminController
{
    protected $deliveryOptionRepository;
    protected $deliveryRegionRepository;
    protected $pageRepository;

    public function __construct(
        OrderRepository $orderRepository,
        DeliveryOptionRepository $deliveryOptionRepository,
        DeliveryRegionRepository $deliveryRegionRepository,
        PageRepository $pageRepository)
    {
        parent::__construct($orderRepository);

        $this->deliveryOptionRepository = $deliveryOptionRepository;

        $this->deliveryRegionRepository = $deliveryRegionRepository;

        $this->pageRepository = $pageRepository;
    }

    public function index(Request $request)
    {
        $search = $request->has('search') ? $request->get('search') : null;

        $models = $this->repository->makeModel()->orderBy('updated_at', 'desc');

        if ($search) {
            $models = $models->where('reference', 'LIKE', '%'.$search.'%');
        }

        $models = $models->paginate();

        return view('panda::modules.orders.index')
            ->with(compact('models', 'search'));
    }

    public function create()
    {
        $model = $this->repository->makeModel();

        $deliveryOptions = $this->deliveryOptionRepository->lists('label', 'id');

        $deliveryRegions = $this->deliveryRegionRepository->lists('region', 'id');

        if (!$model->address) {
            $model->address = new Address();
        }

        return view('panda::modules.orders.create',
            compact('model', 'deliveryOptions', 'deliveryRegions'));
    }

    public function edit(Order $model)
    {
        $deliveryOptions = $this->deliveryOptionRepository->lists('label', 'id');

        $deliveryRegions = $this->deliveryRegionRepository->lists('region', 'id');

        if (!$model->address) {
            $model->address = new Address();
        }

        return view('panda::modules.orders.edit',
            compact('model', 'deliveryOptions', 'deliveryRegions'));
    }

    public function store(Request $request)
    {
        $model = $this->repository->create($request->all());

        return $this->redirect($request, $model);
    }

    public function update(Request $request, Order $model)
    {
        $model = $this->repository->update($request->all(), $model->id);

        return $this->redirect($request, $model);
    }

    public function destroy(Order $model)
    {
        $this->repository->delete($model->id);

        return redirect()->action('Orders\AdminController@index');
    }

    public function dispatch(Order $model)
    {
        $model->sent = 1;
        /*
        Mail::send('emails.dispatched', ['order' => $model], function ($m) use ($model) {
            $m->from('info@scandesignclassic.com', 'Scandesign Classic');
            $name = $model->first_name.' '.$model->last_name;
            $m->to($model->email, $name);
            $m->subject('Scandesign Classic :: Order Dispatched :: '.$model->reference);
        });
        */
        $model->save();

        return redirect()->back();
    }

    public function csv()
    {
        $orders = $this->repository->orderBy('created_at', 'desc')->all();

        $filename = 'orders-' . Carbon::now()->format('d-m-y-H-i') . '.csv';

        $file = public_path() . '/exports/' . $filename;

        $handle = fopen($file, 'w+');

        $columns = [
            'reference' => 'Reference',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'telephone' => 'Telephone',
            'price' => 'Price',
            'delivery_price' => 'Delivery Price',
            'sent' => 'Sent',
            'paid' => 'Paid',
            'created_at' => 'Date'
        ];

        fputcsv($handle, $columns);

        foreach ($orders as $order) {

            $row = [];
            foreach ($columns as $key => $value) {
                $row[$key] = isset($order->{$key}) ? $order->{$key} : null;
            }

            fputcsv($handle, $row);
        }

        fclose($handle);

        $headers = [
            'Content-Type' => 'text/csv'
        ];

        return response()->download($file, $filename, $headers);
    }
}
