<?php

namespace Panda\Http\Controllers\Blocks;

use Illuminate\Http\Request;
use Panda\Models\Block;
use Illuminate\View\View;
use Panda\Forms\BaseForm;
use Panda\Models\TemplateType;
use Illuminate\Routing\Redirector;
use Illuminate\Http\RedirectResponse;
use Panda\Repositories\BlockRepository;
use Panda\Http\Requests\BlockFormRequest;
use Panda\Repositories\TemplateRepository;
use Panda\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
    /**
     * Access to the templates repository.
     *
     * @var TemplateRepository
     */
    protected $templates;

	public function __construct(BlockRepository $repository, TemplateRepository $templates)
	{
		parent::__construct($repository);

        $this->templates = $templates;
	}

    public function index(Request $request)
    {
        $search = $request->has('search') ? $request->get('search') : null;

        $models = $this->repository->makeModel()->orderBy('updated_at', 'desc');

        if ($search) {
            $models->where('title', 'LIKE', '%'.$search.'%');
        }

        $models = $models->paginate();

        return view('panda::modules.blocks.index')
            ->with(compact('models', 'search'));
    }

    /**
     * The block creation form.
     *
     * @return View
     */
	public function create()
    {
        $form = null;

        // Get a new block instance.
        $model = $this->repository->makeModel();

        // Get all block templates and list them by name/ID.
        $templates = $this->templates->findWhere(['template_type_id' => TemplateType::BLOCK])->keyBy('id');

        // Initialises  the dynamic field form, checks to see if an old template ID exists,
        // if not, it will use the template ID from the page model.
        if ($templates->count() > 0) {

            // Get the request object.
            $request = request();

            $template = $request->old('template_id') && isset($templates[$request->old('template_id')])
                ? $templates[$request->old('template_id')]
                : $templates->first();

            $form = new BaseForm($template);

            // Turn the template objects in to an array for the drop-down.
            $templates = $templates->pluck('label', 'id');
        }

        return view('panda::modules.blocks.create',
            compact('model', 'templates', 'form'));
    }

    public function edit(Block $model)
    {
        $form =  null;

        // Get all block templates and list them by name/ID.
        $templates = $this->templates->findWhere(['template_type_id' => TemplateType::BLOCK])->keyBy('id');

        // Initialises  the dynamic field form, checks to see if an old template ID exists,
        // if not, it will use the template ID from the page model.
        if ($templates->count() > 0) {

            // Get the request object.
            $request = request();

            $template = $request->old('template_id') && isset($templates[$request->old('template_id')])
                ? $templates[$request->old('template_id')]
                : $templates[$model->template_id];

            $form = new BaseForm($template);

            // Turn the template objects in to an array for the drop-down.
            $templates = $templates->pluck('label', 'id');
        }

        return view('panda::modules.blocks.edit')
            ->with(compact('model', 'templates', 'form'));
    }

    /**
     * The block store logic.
     *
     * @param BlockFormRequest $request
     * @return Redirector
     */
    public function store(BlockFormRequest $request)
    {
        // Get all POST data.
        $data = $request->all();

        // Create the main block model.
        $model = $this->repository->create($data);

        // If field data exists, attach it to the model.
        if(isset($data['fields'])) {
            $this->repository->setFieldValues($model->id, $data['fields']);
        }

        // Return to the appropriate page.
        return $this->redirect($request, $model);
    }

    /**
     * The block update logic.
     *
     * @param BlockFormRequest $request
     * @param Block $model
     * @return Redirector
     */
    public function update(BlockFormRequest $request, Block $model)
    {
        // Get all PUT data.
        $data = $request->all();

        // Update the main page model.
        $this->repository->update($data, $model->id);

        // If field data exists, attach it to the model.
        if(isset($data['fields'])) {
            $this->repository->setFieldValues($model->id, $data['fields']);
        }

        // Return to the appropriate page.
        return $this->redirect($request, $model);
    }

    /**
     * The block destroy logic.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        // Delete the current page by ID.
        $this->repository->delete($id);

        // Return to the previous page.
        return redirect()->back();
    }
}
