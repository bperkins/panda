<?php

namespace Panda\Http\Controllers\Settings;

use Illuminate\Http\Request;
use Panda\Helpers\FieldHelper;
use Panda\Http\Controllers\BaseAdminController;
use Panda\Repositories\SettingRepository;

class AdminController extends BaseAdminController
{
    public function __construct(SettingRepository $repository)
    {
        parent::__construct($repository);
    }

    public function edit()
    {
        $settings = $this->repository->all();

        $fields = null;

        $order = 0;

        foreach ($settings as $setting) {
            $fields .= FieldHelper::generate($setting->id, $setting->name, $setting->label, $order, 12, null, $setting->field_type_id, $setting->value);
            $order++;
        }

        return view('panda::modules.settings.edit')->with([
            'fields' => $fields
        ]);
    }

    public function update(Request $request)
    {
        $model = null;

        if ($request->has('fields')) {
            foreach ($request->get('fields') as $id => $value) {
                $model = $this->repository->update(['value' => $value], $id);
            }
        }

        return redirect()->route('admin.settings.edit');
    }
}
