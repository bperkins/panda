<?php

namespace Panda\Http\Controllers\Users;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Panda\Http\Controllers\BaseAdminController;
use Panda\Http\Requests\UserFormRequest;
use Panda\Models\Address;
use Panda\Models\User;
use Panda\Repositories\AddressRepository;
use Panda\Repositories\GroupRepository;
use Panda\Repositories\UserRepository;

class AdminController extends BaseAdminController
{
    protected $groupRepository;
    protected $addressRepository;

    public function __construct(UserRepository $repository, GroupRepository $groupRepository, AddressRepository $addressRepository)
    {
        parent::__construct($repository);

        $this->groupRepository = $groupRepository;

        $this->addressRepository = $addressRepository;
    }

    public function index(Request $request)
    {
        $search = $request->has('search') ? $request->get('search') : null;

        $models = $this->repository->makeModel()->orderBy('updated_at', 'desc');

        if ($search) {
            $models = $models->where('email', 'LIKE', '%'.$search.'%');
        }

        $models = $models->paginate();

        return view('panda::modules.users.index')
            ->with(compact('models', 'search'));
    }

    public function create()
    {
        $model = $this->repository->makeModel();

        if (!$model->address) {
            $model->address = new Address();
        }

        $groups = $this->groupRepository->lists('name', 'id');

        return view('panda::modules.users.create')
            ->with(compact('model', 'groups'));
    }

    public function edit(User $model)
    {
        $groups = $this->groupRepository->lists('name', 'id');

        if (!$model->address) {
            $model->address = $this->addressRepository->makeModel();
        }

        return view('panda::modules.users.edit')
            ->with(compact('model', 'groups'));
    }
    
    public function store(UserFormRequest $request)
    {
        $data = $request->all();

        $data['remember_token'] = Str::random(60);

        $model = $this->repository->create($request->all());

        if ($request->has('address')) {
            $address = $this->addressRepository->create($request->get('address'));
            $model->address()->associate($address)->save();
        }

        if ($request->has('groups')) {
            $model->groups()->sync($request->get('groups'));
        }

        return $this->redirect($request, $model);
    }
    
    public function update(UserFormRequest $request, User $model)
    {
        $this->repository->update($request->all(), $model->id);

        if ($request->has('address')) {
            if ($model->address) {
                $model->address->update($request->get('address'));
            } else {
                $model->address->create($request->get('address'));
            }
        }

        $model->groups()->sync($request->get('groups'));

        return $this->redirect($request, $model);
    }

    public function destroy(User $model)
    {
        $model->delete();

        return redirect()->back();
    }
}
