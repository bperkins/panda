<?php

namespace Panda\Http\Controllers\Users;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function index()
    {
        return view('panda::modules.users.login');
    }

    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];

        if(Auth::attempt($credentials)) {
            return redirect()
                ->route('admin.dashboard.index');
        }

        return redirect()
            ->route('admin.auth.index');
    }

    public function logout()
    {
        Auth::logout();

        Session::flush();

        return redirect()
            ->route('admin.auth.login');
    }
}
