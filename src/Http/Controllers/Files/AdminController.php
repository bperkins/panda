<?php

namespace Panda\Http\Controllers\Files;

use Panda\Http\Controllers\BaseAdminController;
use Panda\Models\Template;

class AdminController extends BaseAdminController
{
    public function index()
    {
        return view('panda::modules.files.index');
    }
}
