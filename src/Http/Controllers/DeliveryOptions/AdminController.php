<?php

namespace Panda\Http\Controllers\DeliveryOptions;

use Illuminate\Http\Request;
use Panda\Http\Requests\DeliveryOptionFormRequest;
use Panda\Models\DeliveryOption;
use Panda\Repositories\DeliveryOptionRepository;
use Panda\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
    protected $repository;

    public function __construct(DeliveryOptionRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index(Request $request)
    {
        $search = $request->has('search') ? $request->get('search') : null;

        $models = $this->repository->makeModel()->orderBy('updated_at', 'desc');

        if ($search) {
            $models->where('title', 'LIKE', '%'.$search.'%');
        }

        $models = $models->paginate();

        return view('panda::modules.delivery-options.index')
            ->with(compact('models', 'search'));
    }

    public function create()
    {
        $model = $this->repository->makeModel();

        return view('panda::modules.delivery-options.create',
            compact('model'));
    }

    public function edit(DeliveryOption $model)
    {
        return view('panda::modules.delivery-options.edit',
            compact('model'));
    }

    public function store(DeliveryOptionFormRequest $request)
    {
        $model = $this->repository->create($request->all());

        return $this->redirect($request, $model);
    }

    public function update(DeliveryOptionFormRequest $request, DeliveryOption $model)
    {
        $model = $this->repository->update($request->all(), $model->id);

        return $this->redirect($request, $model);
    }

    public function destroy(DeliveryOption $model)
    {
        $this->repository->delete($model->id);

        return redirect()->action('DeliveryOptions\AdminController@index');
    }
}
