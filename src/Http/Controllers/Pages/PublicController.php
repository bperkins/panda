<?php

namespace Panda\Http\Controllers\Pages;

use Creitive\Breadcrumbs\Breadcrumbs;
use Panda\Repositories\PageRepository;
use Panda\Http\Controllers\BasePublicController;

class PublicController extends BasePublicController
{
    protected $breadcrumbs;

    public function __construct(PageRepository $pageRepository)
    {
        parent::__construct($pageRepository);

        $this->breadcrumbs = $this->getBreadcrumbs();
    }

    public function getPage()
    {
        return $this->pageRepository->makeModel()->getFirstByUri($this->request->path());
    }

    public function getBreadcrumbs()
    {
        $breadcrumbs = new Breadcrumbs();

        $breadcrumbs->setDivider('-&nbsp;');

        $breadcrumbs->addCrumb('Home', '/');

        return $breadcrumbs;
    }
}
