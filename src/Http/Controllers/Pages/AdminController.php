<?php

namespace Panda\Http\Controllers\Pages;

use Illuminate\Http\Request;
use Panda\Http\Requests\PageFormRequest;
use Panda\Models\Page;
use Panda\Forms\BaseForm;
use Panda\Models\Template;
use Panda\Repositories\PageRepository;
use Panda\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
    protected $repository;

    public function __construct(PageRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index(Request $request, Template $template = null)
    {
        if (!$template) {
            abort('404');
        }

        $search = $request->has('search') ? $request->get('search') : null;

        // Initialise a new page model.
        $model = $this->repository->makeModel()->withUnpublished()->orderBy('updated_at', 'desc');

        if ($search) {
            $model = $model->where('title', 'LIKE', '%'.$search.'%');
        }

        // If the template exists, constrain the models.
        if ($template->name !== 'pages') {
            $model = $model->where('template_id', $template->id);
        }

        // Paginate the models.
        $models = $model->paginate();

        return view('panda::modules.pages.index')
            ->with(compact('models', 'template', 'search'));
    }

    public function create(Template $template)
    {
        $model = $this->repository->makeModel();

        $form = new BaseForm($template);

        $parents = null;

        if (!$template->parent_id) {
            $parents = $this->repository->all()->nestList();
        }

        return view('panda::modules.pages.create',
            compact('model', 'form', 'template', 'parents'));
    }

    public function edit(Template $template, $id)
    {
        $model = $this->repository->makeModel()->withUnpublished()->where('id', $id)->first();

        $form = new BaseForm($template);

        $parents = null;

        if (!$template->parent_id) {
            $parents = $this->repository->findWhere(['id' => ['<>', $model->id]])->nestList();
        }

        return view('panda::modules.pages.edit',
            compact('model', 'form', 'template', 'parents'));
    }

    public function store(PageFormRequest $request, Template $template)
    {
        $data = $request->all();

        foreach ($data as $key => $value) {
            if ($value === '') {
                unset($data[$key]);
            }
        }

        if ($template->parents->count() > 0) {
            $data['parent_id'] = $template->parents->first()->id;
        }

        $data['template_id'] = $template->id;

        $model = $this->repository->create($data);

        if(isset($data['fields'])) {
            $this->repository->setFieldValues($model->id, $data['fields']);
        }

        return $this->redirect($request, $model);
    }

    public function update(PageFormRequest $request, Template $template, $id)
    {
        $model = $this->repository->makeModel()->withUnpublished()->where('id', $id)->first();

        $data = $request->all();

        $model = $this->repository->update($data, $model->id);

        if(isset($data['fields'])) {
            $this->repository->setFieldValues($model->id, $data['fields']);
        }

        return $this->redirect($request, $model);
    }

    public function destroy(Template $template, $id)
    {
        $model = $this->repository->makeModel()->withUnpublished()->where('id', $id)->first();

        $this->repository->delete($model->id);

        return redirect('admin/content/'.$template->id);
    }
}
