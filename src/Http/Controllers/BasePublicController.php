<?php

namespace Panda\Http\Controllers;

use Illuminate\Routing\Controller;
use Panda\Repositories\PageRepository;

class BasePublicController extends Controller
{
    protected $pageRepository;
    protected $request;

    public function __construct(PageRepository $pageRepository = null)
    {
        $this->middleware('web');
        $this->pageRepository = $pageRepository;
        $this->request = request();
    }
}