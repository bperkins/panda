<?php

namespace Panda\Http\Controllers\Sitemap;

use Panda\Http\Controllers\BasePublicController;
use Panda\Repositories\PageRepository;

class PublicController extends BasePublicController
{
	protected $pageRepository;

	public function __construct(PageRepository $pageRepository)
	{
		$this->pageRepository = $pageRepository;
	}

	public function index()
	{
		$pages = $this->pageRepository
			->makeModel()
			->whereNotNull('uri')
			->where('status', '=', 1)
			->get();

		$view = view('panda::layouts.sitemap', [
			'pages' => $pages
		]);

		return response()->make($view, 200)
			->header('Content-Type', 'text/xml');
	}
}
