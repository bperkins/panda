<?php

namespace Panda\Http\Controllers\Dashboard;

use Panda\Repositories\PageRepository;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ApiController extends Controller
{
	protected $pageRepository;

	public function __construct(PageRepository $pageRepository)
	{
		$this->pageRepository = $pageRepository;
	}

	public function update(Request $request)
    {
    	$items = $request->get('items', []);

    	if (!$items) {
    		return;
		}

        foreach ($items as $index => $item) {
            $this->pageRepository->setFieldValues($item, [42 => $index]);
        }

		return response()->make('updated', 200);
    }
}
