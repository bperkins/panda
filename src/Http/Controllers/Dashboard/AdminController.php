<?php

namespace Panda\Http\Controllers\Dashboard;

use Panda\Models\Page;
use Panda\Http\Controllers\BaseAdminController;
use Panda\Models\Template;
use Panda\Repositories\OrderRepository;
use Panda\Repositories\PageRepository;
use Panda\Repositories\UserRepository;
use Illuminate\Http\Request;

class AdminController extends BaseAdminController
{
    protected $orderRepository;
    protected $userRepository;
    protected $pageRepository;

    public function __construct(OrderRepository $orderRepository, UserRepository $userRepository, PageRepository $pageRepository)
    {
        parent::__construct();

        $this->orderRepository = $orderRepository;

        $this->userRepository = $userRepository;

        $this->pageRepository = $pageRepository;
    }

    public function index()
    {
        $featured = $this->pageRepository->getFeatured();

        $orderCount = $this->orderRepository->all()->count();

        $dispatched = $this->orderRepository->findWhere(['sent' => 1])->count();

        $dispatchedCount = $orderCount > 0 ? round(($dispatched / $orderCount) * 100) : 0;

        $userCount = $this->userRepository->all()->count();

        $products = $this->pageRepository->findWhere(['template_id' => Template::TEMPLATE_PRODUCT])->sortBy('title');

        $orders = $this->orderRepository->orderBy('created_at', 'desc')->findWhere(['sent' => 0]);

        return view('panda::modules.dashboard.index')->with([
            'orderCount' => $orderCount,
            'dispatchedCount' => $dispatchedCount,
            'userCount' => $userCount,
            'productCount' => $products->count(),
            'orders' => $orders,
            'products' => $products,
            'featured' => $featured,
        ]);
    }

    public function update(Request $request)
    {
        if (!$request->has('featured')) {
            return;
        }

        $page = $this->pageRepository
            ->find($request->get('featured'));

        $featured = $this->pageRepository
            ->getFeatured();

        $featuredCount = $featured->count();

        if ($featuredCount > 3 || $page->template_id != Template::TEMPLATE_PRODUCT) {
            return;
        }

        $this->pageRepository
            ->setFieldValues($page->id, [42 => ($featuredCount + 1)]);

        return redirect()->route('admin.dashboard.index');
    }

    public function destroy($pageId)
    {
        $this->pageRepository
            ->setFieldValues($pageId, [42 => NULL]);

            return redirect()->route('admin.dashboard.index');
    }
}
