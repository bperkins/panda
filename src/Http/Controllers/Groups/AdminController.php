<?php

namespace Panda\Http\Controllers\Groups;

use Illuminate\Http\Request;
use Panda\Models\Group;
use Panda\Repositories\GroupRepository;
use Panda\Http\Requests\GroupFormRequest;
use Panda\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
    public function __construct(GroupRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index(Request $request)
    {
        $search = $request->has('search') ? $request->get('search') : null;

        $models = $this->repository->makeModel()->orderBy('updated_at', 'desc');

        if ($search) {
            $models = $models->where('name', 'LIKE', '%'.$search.'%');
        }

        $models = $models->paginate();

        return view('panda::modules.groups.index')
            ->with(compact('models', 'search'));
    }

    public function create()
    {
        $model = $this->repository->makeModel()->orderBy('updated_at', 'desc');

        return view('panda::modules.groups.create')
            ->with(compact('model'));
    }

    public function edit(Group $model)
    {
        return view('panda::modules.groups.edit')
            ->with(compact('model'));
    }

    public function store(GroupFormRequest $request)
    {
        $model = $this->repository->create($request->all());

        return $this->redirect($request, $model);
    }

    public function update(GroupFormRequest $request, Group $model)
    {
        $this->repository->update($request->all(), $model->id);

        return $this->redirect($request, $model);
    }

    public function destroy(Group $model)
    {
        $model->delete();

        return redirect()->back();
    }
}
