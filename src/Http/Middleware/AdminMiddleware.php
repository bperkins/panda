<?php

namespace Panda\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Panda\Models\Group;

class AdminMiddleware
{
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
dd($user);
        if ($user && !$user->groups->pluck('id')->contains(Group::GROUP_ADMIN)) {
            return redirect()->route('admin.auth.index');
        }

        return $next($request);
    }
}
