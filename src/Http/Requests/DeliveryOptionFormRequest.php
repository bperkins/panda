<?php

namespace Panda\Http\Requests;

class DeliveryOptionFormRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'title' => 'required',
            'price' => 'required|numeric',
            'description' => 'required'
        ];
    }
}
