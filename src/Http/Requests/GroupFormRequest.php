<?php

namespace Panda\Http\Requests;

class GroupFormRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|min:4|max:255|unique:groups,name,'.$this->id
        ];
    }
}