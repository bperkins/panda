<?php

namespace Panda\Http\Requests;

use Panda\Repositories\TemplateRepository;

class BlockFormRequest extends BaseFormRequest
{
    /**
     * Validation attribute names.
     *
     * @var array
     */
    protected $names = [
        'title' => '"Title"',
        'template_id' => '"Template"'
    ];

    /**
     * Validation rules.
     *
     * @var array
     */
    protected $rules = [
        'title' => 'required',
        'template_id' => 'required'
    ];

    /**
     * Set the validation rules for the templates dynamic fields.
     *
     * @param TemplateRepository $repository
     * @return array
     */
    public function rules(TemplateRepository $repository)
    {
        // Check to see if we have a template ID.
        if ($this->request->has('template_id')) {

            // Get the template model.
            $template = $repository->find($this->request->get('template_id'));

            // Loop through all of the templates dynamic fields.
            foreach($template->fields as $field) {

                // If the field has validation, set the rule and name.
                if($field->validation) {
                    $this->names['fields.'.$field->id] = '"'.$field->label.'"';
                    $this->rules['fields.'.$field->id] = $field->validation;
                }
            }
        }

        return $this->rules;
    }

    /**
     * Set the validation attribute names for the templates dynamic fields.
     *
     * @return array
     */
    public function attributes()
    {
        return $this->names;
    }
}
