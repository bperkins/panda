<?php

namespace Panda\Http\Requests;

class UserFormRequest extends BaseFormRequest
{
    public function rules()
    {
        $rules = [
            'email' => 'required|email|max:255|unique:users,email,'.$this->id,
            'password' => 'min:8|max:255|confirmed'
        ];
        
        if (!$this->id) {
            $rules['password'] .= '|required';
        }

        return $rules;
    }
}