<?php

namespace Panda\Traits;

use Panda\Scopes\PublishedScope;

trait PublishedTrait
{
    public static function bootPublishedTrait()
    {
        static::addGlobalScope(new PublishedScope());
    }

    public static function withUnpublished()
    {
        return with(new static)->newQueryWithoutScope(new PublishedScope());
    }
}
