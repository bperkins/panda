<?php

namespace Panda\Seeders;

use Panda\Models\Template;
use Panda\Models\TemplateType;

class TemplateSeeder extends BaseSeeder
{
    public function run()
    {
        $templates = [
            [
                'id' => 1,
                'name' => 'pages',
                'label' => 'Pages',
                'visible' => 1,
                'template_type_id' => TemplateType::PAGE
            ]
        ];

        foreach ($templates as $template) {
            Template::create($template);
        }
    }
}