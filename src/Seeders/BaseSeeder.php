<?php

namespace Panda\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class BaseSeeder extends Seeder
{
    protected $timestamp;

    public function __construct()
    {
        $this->timestamp = Carbon::now();
    }

    public function run() { }
}