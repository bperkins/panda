<?php

namespace Panda\Seeders;

use Illuminate\Database\Seeder;

class InstallSeeder extends Seeder
{
    public function run()
    {
        $this->call(GroupSeeder::class);

        $this->call(TemplateTypeSeeder::class);

        $this->call(TemplateSeeder::class);

        $this->call(FieldTypeSeeder::class);

        $this->call(DeliveryOptionSeeder::class);

        $this->call(DeliveryRegionSeeder::class);
    }
}
