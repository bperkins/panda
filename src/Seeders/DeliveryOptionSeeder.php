<?php

namespace Panda\Seeders;

use Panda\Models\DeliveryOption;

class DeliveryOptionSeeder extends BaseSeeder
{
    public function run()
    {
        $deliveryOptions = [
            [
                'id' => 1,
                'name' => 'delivery',
                'label' => 'Delivery'
            ],
            [
                'id' => 2,
                'name' => 'courier',
                'label' => 'Courier'
            ],
            [
                'id' => 3,
                'name' => 'collection',
                'label' => 'Collection'
            ]
        ];

        foreach ($deliveryOptions as $deliveryOption) {
        	DeliveryOption::create($deliveryOption);
        }
    }
}
