<?php

namespace Panda\Seeders;

use Panda\Models\TemplateType;

class TemplateTypeSeeder extends BaseSeeder
{
    public function run()
    {
        $templateTypes = [
            [
                'id' => 1,
                'name' => 'Page'
            ],
            [
                'id' => 2,
                'name' => 'Block'
            ]
        ];

        foreach ($templateTypes as $templateType) {
            TemplateType::create($templateType);
        }
    }
}