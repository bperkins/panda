<?php

namespace Panda\Seeders;

use Panda\Models\DeliveryRegion;

class DeliveryRegionSeeder extends BaseSeeder
{
    public function run()
    {
        $deliveryRegions = [
            [
                'id' => 1,
                'region' => 'Greater London',
                'delivery' => 50.00,
                'courier' => 50.00
            ],
            [
                'id' => 2,
                'region' => 'Canterbury',
                'delivery' => 75.00,
                'courier' => 75.00
            ],
            [
                'id' => 3,
                'region' => 'Brighton',
                'delivery' => 75.00,
                'courier' => 75.00
            ],
            [
                'id' => 4,
                'region' => 'Southampton',
                'delivery' => 75.00,
                'courier' => 75.00
            ],
            [
                'id' => 5,
                'region' => 'Swindon',
                'delivery' => 75.00,
                'courier' => 75.00
            ],
            [
                'id' => 6,
                'region' => 'Oxford',
                'delivery' => 75.00,
                'courier' => 75.00
            ],
            [
                'id' => 7,
                'region' => 'Luton',
                'delivery' => 75.00,
                'courier' => 75.00
            ],
            [
                'id' => 8,
                'region' => 'Colchester',
                'delivery' => 75.00,
                'courier' => 75.00
            ],
            [
                'id' => 9,
                'region' => 'Bristol',
                'delivery' => 90.00,
                'courier' => 90.00
            ],
            [
                'id' => 10,
                'region' => 'Bournemouth',
                'delivery' => 90.00,
                'courier' => 90.00
            ],
            [
                'id' => 11,
                'region' => 'Dover',
                'delivery' => 90.00,
                'courier' => 90.00
            ],
            [
                'id' => 12,
                'region' => 'Ipswich',
                'delivery' => 90.00,
                'courier' => 90.00
            ],
            [
                'id' => 13,
                'region' => 'Cambridge',
                'delivery' => 90.00,
                'courier' => 90.00
            ],
            [
                'id' => 14,
                'region' => 'Milton Keynes',
                'delivery' => 90.00,
                'courier' => 90.00
            ],
            [
                'id' => 15,
                'region' => 'Cheltenham',
                'delivery' => 90.00,
                'courier' => 90.00
            ],
            [
                'id' => 16,
                'region' => 'Birmingham',
                'delivery' => 110.00,
                'courier' => 110.00
            ],
            [
                'id' => 17,
                'region' => 'Leicester',
                'delivery' => 110.00,
                'courier' => 110.00
            ],
            [
                'id' => 18,
                'region' => 'Peterborough',
                'delivery' => 110.00,
                'courier' => 110.00
            ],
            [
                'id' => 19,
                'region' => 'Norwich',
                'delivery' => 110.00,
                'courier' => 110.00
            ],
            [
                'id' => 20,
                'region' => 'Exeter',
                'delivery' => 140.00,
                'courier' => 140.00
            ],
            [
                'id' => 21,
                'region' => 'Nottingham',
                'delivery' => 140.00,
                'courier' => 140.00
            ],
            [
                'id' => 22,
                'region' => 'Liverpool',
                'delivery' => 180.00,
                'courier' => 180.00
            ],
            [
                'id' => 23,
                'region' => 'Manchester',
                'delivery' => 180.00,
                'courier' => 180.00
            ],
            [
                'id' => 24,
                'region' => 'Leeds',
                'delivery' => 220.00,
                'courier' => 220.00
            ],
            [
                'id' => 25,
                'region' => 'Hull',
                'delivery' => 220.00,
                'courier' => 220.00
            ],
            [
                'id' => 26,
                'region' => 'Newcastle',
                'delivery' => 250.00,
                'courier' => 250.00
            ],
            [
                'id' => 27,
                'region' => 'Glasgow',
                'delivery' => 300.00,
                'courier' => 300.00
            ],
            [
                'id' => 28,
                'region' => 'Edinburgh',
                'delivery' => 300.00,
                'courier' => 300.00
            ],
            [
                'id' => 29,
                'region' => 'Aberdeen',
                'delivery' => 350.00,
                'courier' => 350.00
            ],
            [
                'id' => 30,
                'region' => 'Sheffield',
                'delivery' => 160.00,
                'courier' => 160.00
            ],
            [
                'id' => 31,
                'region' => 'Bradford',
                'delivery' => 220.00,
                'courier' => 220.00
            ],
            [
                'id' => 32,
                'region' => 'Portsmouth',
                'delivery' => 90.00,
                'courier' => 90.00
            ],
            [
                'id' => 33,
                'region' => 'Coventry',
                'delivery' => 110.00,
                'courier' => 110.00
            ],
            [
                'id' => 34,
                'region' => 'Derby',
                'delivery' => 140.00,
                'courier' => 140.00
            ],
            [
                'id' => 35,
                'region' => 'York',
                'delivery' => 220.00,
                'courier' => 220.00
            ],
            [
                'id' => 36,
                'region' => 'Plymouth',
                'delivery' => 160.00,
                'courier' => 160.00
            ],
            [
                'id' => 37,
                'region' => 'Wolverhampton',
                'delivery' => 130.00,
                'courier' => 130.00
            ],
            [
                'id' => 38,
                'region' => 'Stoke-on-Trent',
                'delivery' => 150.00,
                'courier' => 150.00
            ],
            [
                'id' => 39,
                'region' => 'Sunderland',
                'delivery' => 250.00,
                'courier' => 250.00
            ],
            [
                'id' => 40,
                'region' => 'Gloucester',
                'delivery' => 110.00,
                'courier' => 110.00
            ],
            [
                'id' => 41,
                'region' => 'Bath',
                'delivery' => 90.00,
                'courier' => 90.00
            ],
            [
                'id' => 42,
                'region' => 'Reading',
                'delivery' => 60.00,
                'courier' => 60.00
            ],
            [
                'id' => 43,
                'region' => 'Luton',
                'delivery' => 80.00,
                'courier' => 80.00
            ],
            [
                'id' => 44,
                'region' => 'Blackpool',
                'delivery' => 210.00,
                'courier' => 210.00
            ],
            [
                'id' => 45,
                'region' => 'Worcester',
                'delivery' => 120.00,
                'courier' => 120.00
            ],
            [
                'id' => 46,
                'region' => 'Canterbury',
                'delivery' => 80.00,
                'courier' => 80.00
            ],
            [
                'id' => 47,
                'region' => 'Lincoln',
                'delivery' => 160.00,
                'courier' => 160.00
            ],
            [
                'id' => 48,
                'region' => 'Chester',
                'delivery' => 180.00,
                'courier' => 180.00
            ],
            [
                'id' => 49,
                'region' => 'Preston',
                'delivery' => 200.00,
                'courier' => 200.00
            ],
            [
                'id' => 50,
                'region' => 'Dundee',
                'delivery' => 330.00,
                'courier' => 330.00
            ],
            [
                'id' => 51,
                'region' => 'Cardiff',
                'delivery' => 110.00,
                'courier' => 110.00
            ],
            [
                'id' => 52,
                'region' => 'Newport',
                'delivery' => 110.00,
                'courier' => 110.00
            ],
            [
                'id' => 53,
                'region' => 'Swansea',
                'delivery' => 140.00,
                'courier' => 140.00
            ]
        ];

        foreach ($deliveryRegions as $deliveryRegion) {
            DeliveryRegion::create($deliveryRegion);
        }
    }
}
