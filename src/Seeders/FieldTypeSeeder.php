<?php

namespace Panda\Seeders;

use Panda\Models\FieldType;

class FieldTypeSeeder extends BaseSeeder
{
    public function run()
    {
        $fieldTypes = [
            [
                'id' => 1,
                'name' => 'Block Field',
                'class' => 'Panda\Fields\BlockField',
                'view' => 'block-field'
            ],
            [
                'id' => 2,
                'name' => 'Image Field',
                'class' => 'Panda\Fields\ImageField',
                'view' => 'image-field'
            ],
            [
                'id' => 3,
                'name' => 'Select Field',
                'class' => 'Panda\Fields\SelectField',
                'view' => 'select-field'
            ],
            [
                'id' => 4,
                'name' => 'Text Field',
                'class' => 'Panda\Fields\TextField',
                'view' => 'text-field'
            ],
            [
                'id' => 5,
                'name' => 'Wysiwyg Field',
                'class' => 'Panda\Fields\WysiwygField',
                'view' => 'wysiwyg-field'
            ],
            [
                'id' => 6,
                'name' => 'Text Area Field',
                'class' => 'Panda\Fields\TextAreaField',
                'view' => 'text-area-field'
            ],
            [
                'id' => 7,
                'name' => 'Checkbox Field',
                'class' => 'Panda\Fields\CheckboxField',
                'view' => 'checkbox-field'
            ]
        ];

        foreach ($fieldTypes as $fieldType) {
            FieldType::create($fieldType);
        }
    }
}
