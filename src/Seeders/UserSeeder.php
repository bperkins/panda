<?php

namespace Panda\Seeders;

use Illuminate\Support\Str;
use Panda\Models\User;

class UserSeeder extends BaseSeeder
{
    public function run()
    {
        $users = [
            [
                'id' => 1,
                'first_name' => 'Ben',
                'last_name' => 'Perkins',
                'email' => 'b.c.m.perkins@gmail.com',
                'telephone' => '1234567890',
                'password' => 'secret',
                'remember_token' => Str::random(60)
            ]
        ];

        foreach ($users as $user) {
            User::create($user);
        }
    }
}
