<?php

namespace Panda\Seeders;

use Panda\Models\Group;

class GroupSeeder extends BaseSeeder
{
    public function run()
    {
        $groups = [
            [
                'id' => 1,
                'name' => 'Admin'
            ],
            [
                'id' => 2,
                'name' => 'User'
            ]
        ];

        foreach ($groups as $group) {
            Group::create($group);
        }
    }
}