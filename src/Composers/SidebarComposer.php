<?php

namespace Panda\Composers;

use Illuminate\View\View;
use Panda\Models\TemplateType;
use Panda\Repositories\TemplateRepository;

class SidebarComposer
{
    protected $templates;

    public function __construct(TemplateRepository $repository)
    {
        $this->templates = $repository->makeModel()->where('template_type_id', TemplateType::PAGE)->where('visible', '<>', 0)->orderBy('visible')->get();
    }

    public function compose(View $view)
    {
        $view->with('templates', $this->templates);
    }
}