<?php

namespace Panda\Fields;

use Panda\Models\Field;
use Panda\Models\Page;
use Panda\Models\Template;
use Panda\Models\TemplateType;

class SelectField extends BaseField
{
    public function __construct(Field $field, $value, $model = null)
    {
        parent::__construct($field, $value, $model);

		$data = [];

		$multiple = false;

    	$options = $field->options->pluck('value', 'option');

    	if (isset($options['template_id'])) {

    		$template = Template::find($options['template_id']);

            $pages = $template->template_type_id === TemplateType::PAGE ? $template->pages : $template->blocks;

            if (!is_null($pages)) {
                $data = $pages->sortBy('title')
                    ->pluck('title', 'id');
            }

    	} else if (isset($options['json'])) {
    		$data = json_decode($options['json']);
    	}

		if (isset($options['parent_id'])) {
			$this->setName('parent_id');

            if ($model) {
                $this->setValue($model->parent_id);
            }
		}
		
		if (isset($options['multiple'])) {
			$multiple = true;
			$this->setName($this->attributes['name'].'[]');
		}

        $this->setAttributes(compact('data', 'multiple', 'name'));
    }
}
