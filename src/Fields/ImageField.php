<?php

namespace Panda\Fields;

use Panda\Models\Field;

class ImageField extends BaseField
{
    public function __construct(Field $field, $value, $model = null)
    {
        parent::__construct($field, $value, $model);

        $images = [];

        if($value) {
            $images = json_decode($value);
        }
        
        $this->setAttributes(compact('images'));
    }
}
