<?php

namespace Panda\Fields;

use Illuminate\View\View;
use Panda\Models\Field;
use Panda\Models\Page;

class BaseField
{
    /**
     * The view object associated with the field.
     *
     * @var View
     */
    protected $view;

    protected $attributes;

    protected $value;

    public function __construct(Field $field, $value, $model = null)
    {
        $this->value = $value;

        $this->attributes = [
            'field' => $field,
            'value' => old('fields.'.$field->id) ? old('fields.'.$field->id) : $value,
            'name' => 'fields['.$field->id.']',
        ];
        
        $this->view = view('panda::fields.'.$field->type->view);
    }

    /**
     * Return the HTML output of the view.
     *
     * @return string
     */
    public function render()
    {
        return $this->view->with($this->attributes)->render();
    }

    protected function setAttributes(array $attributes)
    {
        $this->attributes = array_merge($this->attributes, $attributes);
    }

    protected function setName($name)
    {
        $this->attributes['name'] = $name;
    }

    public function setValue($value)
    {
        $this->attributes['value'] = $value;
    }
}
