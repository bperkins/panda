<?php

namespace Panda\Fields;

use Panda\Models\Field;

class TextField extends BaseField
{
    public function __construct(Field $field, $value, $model = null)
    {
        parent::__construct($field, $value, $model);
    }
}