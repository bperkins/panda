<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('label');
            $table->string('validation')->nullable();
            $table->tinyInteger('hidden')->unsigned()->default(0);
            $table->tinyInteger('order')->unsigned()->default(0);
            $table->tinyInteger('size')->unsigned()->default(12);
            $table->integer('field_type_id')->unsigned();
            $table->integer('template_id')->unsigned();
            $table->timestamps();
            $table->foreign('field_type_id')->references('id')->on('field_types')->onDelete('cascade');
            $table->foreign('template_id')->references('id')->on('templates')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fields');
    }
}
