<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templates', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('label');
            $table->string('icon')->nullable();
            $table->integer('visible');
            $table->boolean('has_parent')->default(1);
            $table->boolean('has_uri')->default(1);
            $table->integer('template_type_id')->unsigned();
            $table->timestamps();
            $table->foreign('template_type_id')->references('id')->on('template_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('templates');
    }
}
