<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_items', function(Blueprint $table) {
            $table->increments('id');
            $table->string('item');
            $table->string('lot_number');
            $table->float('price');
            $table->integer('page_id')->unsigned()->nullable();
            $table->integer('order_id')->unsigned();
            $table->timestamps();
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('set null');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_items');
    }
}
