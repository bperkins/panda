<?php

namespace Panda\Observers;

class TouchObserver
{
    public function saving($model)
    {
        //$user = auth()->user();

        if (!$model->id) {
            $model->created_by = 1;
        }

        $model->updated_by = 1;
    }
}