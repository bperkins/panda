<?php

namespace Panda\Observers;

use Panda\Models\Page;

class UriObserver
{
    public function saving(Page $model)
    {
        if (!$model->template->has_uri) {
            return;
        }

        $parentUri = null;
        if($parentPage = $model->parent) {
            $parentUri = $parentPage->uri;
        }

        if($parentUri) {
            $model->uri = $parentUri === '/' ? '' : $parentUri;
            if($model->slug) {
                $model->uri .= '/'.$model->slug;
            }
        } else {
            $model->uri = '/'.$model->slug;
        }

        $originalUri = $model->uri;
        $originalSlug = $model->slug;

        $i = 0;

        while($this->uriExists($model)) {
            $i++;
            $model->uri = $originalUri.'-'.$i;
            $model->slug = $originalSlug.'-'.$i;
        }
    }

    private function uriExists(Page $model)
    {
        $query = $model->where('uri', $model->uri);

        if($model->id) {
            $query->where('id', '!=', $model->id);
        }

        return $query->first() ? true : false;
    }
}
