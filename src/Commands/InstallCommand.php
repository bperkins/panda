<?php

namespace Panda\Commands;

use DB;
use Illuminate\Console\Command;
use Panda\Models\User;
use Panda\Models\FieldType;
use Panda\Models\Setting;
use Panda\Repositories\UserRepository;
use Panda\Repositories\SettingRepository;

class InstallCommand extends Command
{
    /**
     * @var string $name
     */
    protected $name = 'cms:install';

    /**
     * @var string $description
     */
    protected $description = 'Installs the default platform.';

    /**
     * @var UserRepository $userRepository
     */
    protected $userRepository;

    /**
     * @var SettingRepository $settingRepository
     */
    protected $settingRepository;

    /**
     * @var string $siteName;
     */
    protected $siteName;

    /**
     * The default firing function to install the CMS.
     * @param UserRepository $userRepository
     * @return void
     */
    public function fire(UserRepository $userRepository, SettingRepository $settingRepository)
    {
        $this->userRepository = $userRepository;
        $this->settingRepository = $settingRepository;

        // Ensure the user has ran composer.
        if (!$this->confirm('Have you ran composer?')) {
            $this->error('Please run composer before proceeding.');
            return;
        }

        // Ensure the user has entered their database connection details.
        if (!$this->confirm('Have you entered your database connection details?')) {
            $this->error('Please enter your database connection details before proceeding.');
            return;
        }

        // Insert the default settings.
        $this->insertSettings();

        // Ask the user if they wish to migrate and seed.
        if ($this->confirm('Migrate and seed database?')) {
            $this->databaseMigrateSeed();
        }

        // Ask the user if they wish to insert a default admin account.
        if ($this->confirm('Insert default admin account?')) {
            $this->insertAdminUser();
        }
    }

    /**
     * Insert default CMS settings.
     * @return void
     */
    private function insertSettings()
    {
        // Ask the user for the site name.
        $siteName = $this->ask('Site name');

        $data = [
            'name' => Setting::SETTING_SITE_NAME,
            'label' => 'Site name',
            'value' => $siteName,
            'field_type_id' => FieldType::FIELD_TYPE_TEXT_FIELD
        ];

        // Insert the provided site name as a setting.
        $this->settingRepository->create($data);
    }

    /**
     * Migrate and seed the database.
     * @return void
     */
    private function databaseMigrateSeed()
    {
        // Migrate the database using the CMS path.
        $this->call('migrate', ['--path' => 'cms/src/database/migrations']);
        $this->info('Database migrated successfully...');

        // Seed the database using the CMS classes.
        $this->call('db:seed', ['--class' => 'Panda\Seeders\InstallSeeder']);
        $this->info('Database seeded successfully...');
    }

    /**
     * Creates the default admin user account.
     * @return void
     */
    private function insertAdminUser()
    {
        // Ask for the users first name.
        $firstName = $this->ask('First name');

        // Ask for the users last name.
        $lastName = $this->ask('Last name');

        // Ask for the users email address.
        $emailAddress = $this->ask('Email address');

        // Ask for the users password.
        $password = $this->secret('Password');

        $data = [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $emailAddress,
            'password' => bcrypt($password)
        ];

        // Create the user account.
        $user = $this->userRepository->create($data);

        // Sync the user to the admin group.
        $user->groups()->sync(Group::GROUP_ADMIN);

        $this->info('Admin user inserted successfully...');
    }
}
