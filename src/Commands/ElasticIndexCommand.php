<?php

namespace Panda\Commands;

use Illuminate\Console\Command;
use Panda\Repositories\PageRepository;
use Elasticsearch\ClientBuilder;

class ElasticIndexCommand extends Command
{
    const ELASTIC_INDEX = 'scandesignclassic';

    /**
     * @var string $name
     */
    protected $name = 'cms:elastic:index';

    /**
     * @var string $description
     */
    protected $description = 'Indexes Elasticsearch.';

    /**
     * Index Elasticsearch.
     * @param UserRepository $userRepository
     * @return void
     */
    public function fire(PageRepository $pageRepository)
    {
        $pages = $pageRepository->all();

        $client = ClientBuilder::create()->build();

        foreach ($pages as $page) {
            $client->index([
                'index' => self::ELASTIC_INDEX,
                'type' => 'pages',
                'id' => $page->id,
                'body' => $page->toArray()
            ]);

            $this->info('Indexed '.$page->title.' ('.$page->id.')');
        }
    }
}
