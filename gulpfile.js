var gulp     = require('gulp'),
    less     = require('gulp-less'),
    rename   = require('gulp-rename'),
    watch    = require('gulp-watch'),
    concat   = require('gulp-concat'),
    uglify   = require('gulp-uglify');

gulp.task('less', function() {
    return gulp.src('src/resources/assets/less/master.less')
        .pipe(less())
        .pipe(rename('panda.css'))
        .pipe(gulp.dest('../public/css'));
});

gulp.task('js', function() {
    var files = [
        'node_modules/jquery/dist/jquery.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/alertify.js/dist/js/alertify.js',
        'node_modules/fancybox/dist/js/jquery.fancybox.js',
        'src/resources/assets/js/*.js',
        'src/resources/assets/js/fields/*.js'
    ];
    return gulp.src(files)
        .pipe(concat('panda.js'))
        .pipe(uglify())
        .pipe(rename('panda.min.js'))
        .pipe(gulp.dest('../public/js'));
});

gulp.task('fonts', function() {
    return gulp.src([
        'node_modules/font-awesome/fonts/*'
    ])
    .pipe(gulp.dest('../public/fonts'));
});

gulp.task('fancybox', function() {
    return gulp.src('node_modules/fancybox/dist/img/*')
        .pipe(gulp.dest('../public/components/fancybox/source'));
});

gulp.task('watch', function() {
    gulp.watch('src/resources/assets/less/*.less', ['less']);
    gulp.watch('src/resources/assets/js/**/*.js', ['js']);
});

gulp.task('all', [
    'less',
    'js',
    'fancybox',
    'fonts',
    'watch'
]);

gulp.task('default', [
    'less',
    'js',
    'watch'
]);
